	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});

	// AGREGAR PRODUCTOS AL PEDIDO EN LA TABLA -->
	
	function agregar_pedido() {
        var id_producto=document.getElementById("id_pro").value;
        var descripcion = document.getElementsByName("producto")[0].value;
        var cantidad = document.getElementsByName("cantidad")[0].value;
        var valor_u = document.getElementById("precio_u").innerText;
        var subtotal = document.getElementById("subtotal").innerText;

        var id_producto=$("#id_pro").val();
	    var descripcion = document.getElementsByName("producto")[0].value;
	    var cantidad = document.getElementsByName("cantidad")[0].value;
	    var valor_u = $("#precio_u").html();
	    var subtotal = $("#subtotal").html();

        var num_item = document.getElementsByTagName("td").length;
        var retirar  = "<img class=\"eliminar_pedido\" src=\"../images/icon_svg/delete84.svg\" width=\"20em\" height=\"20em\" title=\"Eliminar del Pedido\">";

        var table = $("#body_t_pedido").append("<tr class=\"prod_ped\"><td>"+descripcion+"</td><td>"+cantidad+"</td><td>"+valor_u+"</td><td class='subtotal_pro'>"+subtotal+"</td><td>"+retirar+"</td><td style='display:none' class='valores_producto'>"+id_producto+"|"+cantidad+"</td></tr>");

         $(".list_funcionarios").attr("disabled",true);
        $(".list_areas").attr("disabled",true);
        $(".enviar_ped").attr("disabled", false);
        $(".cantidad_prod").attr("value",1);
        $(".cantidad_prod").attr("disabled", true);
        $('.desc_prod option:eq(0)').prop('selected', true);
        $('#search_producto').text('');
        $(".precio_u").html("");

        calcula_total();
	}
    

// PRECIO DE PRODUCTO -->
    
    function get_precio(str) {
        if (str=="") {
            $("#precio_u").html()="";
            return;
        }
        else{
            $.post("get_pre.php",{p_u:str},function(datos,status){
                //alert(datos);
                var valores = datos.split("|");
                //alert(valores[0]);
                $("#precio_u").html(valores[0]);
                $("#id_pro").val(valores[1]);
                //$(".cantidad_prod").attr("value","");
                $(".subtotal_ped").html("");
                $(".agregar_prod").attr("disabled", true);

                //calcula_total();
                get_pre_subtotal(1);
            });
        }
    }
    

// BUSQUEDA DE FUNCIONADIO -->
    
        function get_funcionario(str) {
            if (!str.length) {
                $("#nombre").html("...");
                return;
            } else{
                $.post("get_us.php",{id_u:str},function(response){
                    $("#nombre").html("");

                    if (response!="") {                        
                         $("#nombre").html(response);
                    }
                });
            }


        /*if (!str.length) {
            $("#nombre").html("...");
                return;
            } else{
                $.post("get_us.php",{id_u:str},function(response){
                $("#nombre").html("");

                if (response!="") {                        
                    var nombres=response.split(",");
                    $("#nombre").css("position","absolute");

                    for (var i = 0; i < nombres.length; i++) {
                        $("#nombre").append("<option name='nombre_us' id='name_us' value='"+nombres[0]+"'>"+nombres[0]+"</option><br>");
                    }
                }
                else{
                    alert ("No se encontraron Coincidencias");
                }
            });
        }*/

        }
    

// BUSQUEDA DE FUNCIONADIOS PARA PEDIDOS -->
    
        function get_funcionarios(str, name = "") {
            console.log(name)
        if (!str.length) {
            $("#list_funcionarios").html("Seleccione el área");
                return;
            } else{
            $.post("get_us.php",{id_u:str,opc:1, name: name},function(response){

                if (response!="") {
                     $("#list_funcionarios").html("");
                    if ($("#list_funcionarios").html()=="Selecione el área") {
                        $(".desc_prod").attr("disabled", true);
                        $(".view_filtro_user").removeClass('hide');
                    }else{
                        $(".desc_prod").attr("disabled", true);
                        $(".view_filtro_user").addClass('hide');
                        var nombres=response.split("|");
                        $("#list_funcionarios").html("");
                        for (var i = 0; i < nombres.length; i++) {
                            
                            var nom_cedu=nombres[i].split("-");
                            
                            for (var x = 0; x < (nom_cedu.length-1); x++) {
                                //alert(nom_cedu[0]);
                                $("#list_funcionarios").append("<option value='"+nom_cedu[0]+"'>"+nom_cedu[1]+"</option>");
                                //break;
                            }
                        };
                    }
                }
                else{
                    $(".desc_prod").attr("disabled", true);
                    $(".cantidad_prod").attr("disabled", true);
                    $(".agregar_prod").attr("disabled", true);

                    $(".desc_prod").attr("value","Elija uno");
                    $(".cantidad_prod").attr("value","");
                    $(".precio_u").html("");
                    $(".subtotal_ped").html("");
                };
            });
        }
    }
    

    
    
    function get_id_usuario(und) {
        var id_usuario=und;
       

        $("#id_usuario").val(id_usuario);
    }
    

// PRECIO DE SUBTOTAL -->
    
    function get_pre_subtotal(und) {
        var patron = /^\d*$/; //Expresión regular para aceptar solo números enteros

        if(und=="" || und=="0"){
            $(".agregar_prod").attr("disabled",true);
            var estado=document.getElementById("subtotal").innerHTML="NaN";
        }else if (und=="NaN"){
            $(".agregar_prod").attr("disabled",true);
            var estado=document.getElementById("subtotal").innerHTML="NaN";
        }else if(!patron.test(und)){
            $(".agregar_prod").attr("disabled",true);
            var estado=document.getElementById("subtotal").innerHTML="NaN";
        }else{
            var precio_u = parseInt(document.getElementById("precio_u").innerText);
            var c_subtotal=precio_u*parseInt(und);
            var estado=document.getElementById("subtotal").innerHTML=c_subtotal;
            $(".agregar_prod").attr("disabled",false);
        }
    }
    


// PRECIO DE TOTAL PARA LA VENTA DE PEDIDO-->
    
    function calcula_total() {
        var suma=0;
        $(".subtotal_pro").each(function(index){
           var valor=$(this).html();
           suma+=parseInt(valor);
        });

        $("#total_id").html(suma);
        $("#cantidad").val("");
        $("#subtotal").html("");
        
        btn_enviar();
        $(".agregar_prod").attr("disabled",true);
    }
    

// PRECIO DE TOTAL PARA LA VISTA DE FUNCIONARIOS DEUDORES-->
    
        function calcula_total_fun() {
            var suma=0;
            $(".subtotal_ped_fun").each(function(index){
                var valor=$(this).html();
                suma+=parseInt(valor);
            });

            $("#total_ped_fun").html('$' + parseFloat(suma, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
        }
    

    // PRECIO DE TOTAL PARA LA VISTA DE FUNCIONARIOS DEUDORES POR BUSQUEDA-->
    
        function calcula_total_fun_busq() {
            var suma_b=0;
            $(".subtotal_ped_fun_busq").each(function(index){
                var valor_b=$(this).html();
                suma_b+=parseInt(valor_b);
            });

            $("#total_ped_fun_busq").html('$' + parseFloat(suma_b, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            $("#lista_pago_func").css("display","none");
        }
    

    
        function calcula_total_ped() {
            var suma_b=0;
            $(".calcula_total_ped").each(function(index){
                var valor_b=$(this).html();
                suma_b+=parseInt(valor_b);
            });

            $("#total_ped_fun_busq").html('$' + parseFloat(suma_b, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            $("#lista_pago_func").css("display","none");
        }
    

    // INGRESAR PEDIDO DEFINITIVO DEL USUARIO -->

    
        function ingresar_pedido() {
            $(".enviar_ped").attr("disabled", true);

            //// FUNCION DE PRECARGA-->
            //Añadimos la imagen de carga en el contenedor
            $('#t_pedido').css("display","none")
            //$('#precarga').css("position","relative");
            $('#precarga').html('<center><img class="img-circle" src="../images/loader.gif" width="50em" height="50em" /></center>');
            //// FIN FUNCION DE PRECARGA-->

            var ids=new Array();
            var cantidad=new Array();
            var a=0;
            $(".valores_producto").each(function(){
               var valores=$(this).html().split("|");
               ids[a]=valores[0];
               cantidad[a]=valores[1];
               a++;
            });
            //alert(ids);

            var x_ides=ids.join(",");
            var x_cantidad=cantidad.join(",");
            var x_valor=$("#total_id").html();
            var x_id_user=$("#id_usuario").val();

            //alert(x_id_user);

            $.post("ins_ped.php",{ides:x_ides, cantidades: x_cantidad, valor: x_valor, id_user: x_id_user},function(data){
                
                //alert(data);
                if (data=='pedidoOKsaldoOK') {
                    alert("Se ha registrado la compra con éxito.");
                }else if (data=='pedidoOK') {
                    alert("Se ha registrado el pedido, pero ocurrió un problema al actualizar el saldo, favor informar a TICs...");
                }else{
                    alert("Error al Registrar el Pedido");
                }
                
                location.reload();
                //location.href="pedidos.php";
            });
        }
    

// MOSTRAR PEDIDOS POR USUARIO Y FECHA -->
    
        function pedidos_usuario_fecha() {
            var x_user=$("#cedula").val();
            var f_ini=$(".f_inicio").val();
            var f_fin=$(".f_fin").val();

            //alert(f_ini);
            //CONFIGURAR PARA CARGAR VISTA DE SOLO LA BUSQUEDA INDICADA SIN CEDULA
            //-----------------------------------------------------------------------
            if (!x_user) {

                //alert("Vacio: "+x_user);

                //registro_pagos("Vacio:"+x_user);

                $("#lista_pago_func").css("display", "none");
                $(".volver_ing_pag").css("display", "none");
                $("#lista_pago_func_fecha").css("display", "block");
                $(".volver_ped").css("display", "block");
                
                $("#t_body_lpendientes_original").children().remove();

                $.post("get_ped_us.php",{user:x_user,fecha_ini:f_ini,fecha_fin:f_fin, bandera:'con_fecha_rango'},function(dato1,status){
                    row_dato1=dato1.split(";");

                    for (var i = 0; i < row_dato1.length; i++) {
                        var row_dato1_f=row_dato1[i].split("|");
                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                            //alert(row_dato1_f[0]);
                            $("#t_body_lpendientes_original").append("<tr id=\"pendiente-"+row_dato1_f[0]+"\"><td class=\"id_ped_fecha\">"+row_dato1_f[0]+"</td><td>"+row_dato1_f[1]+" "+row_dato1_f[2]+"</td><td class=\"id_fecha_fecha\">"+row_dato1_f[3]+"</td><td class='subtotal_ped_fun_busq pendiente-"+row_dato1_f[0]+" '>"+row_dato1_f[4]+"</td><td><img class=\"pagar\" src=\"../images/icon_svg/money175.svg\" width=\"25em\" height=\"25em\" title=\"Realizar Pago\">    <img class=\"detalle_ped\" src=\"../images/icon_svg/write13.svg\" width=\"25em\" height=\"25em\" title=\"Ver Detalles\"></td></tr>");
                            break;
                        }
                    };
                    calcula_total_fun_busq();
                });
            }else{
                //alert("Con algo: "+x_user);
                registro_pagos(x_user);

               /* $(".volver_ped").css("display", "none");
                $("#lista_pago_func_busq").css("display", "none");
                $("#lista_pago_func_fecha").css("display", "none");
                $("#lista_por_funcionario").css("display", "block");
                $(".volver_ing_pag").css("display", "block");*/


                $("#lista_pago_func").css("display", "none");
                $(".volver_ing_pag").css("display", "none");
                $("#lista_pago_func_fecha").css("display", "block");
                $(".volver_ped").css("display", "block");
                
                $("#t_body_lpendientes_original").children().remove();

                $.post("get_ped_us.php",{user:x_user,fecha_ini:f_ini,fecha_fin:f_fin, bandera:'con_fecha'},function(dato1,status){
                    row_dato1=dato1.split(";");

                    /*for (var i = 0; i < row_dato1.length; i++) {
                        var row_dato1_f=row_dato1[i].split("|");
                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                            $("#body_t_pedido_usuario").append("<tr><td class=\"id_ped_fecha\">"+row_dato1_f[0]+"</td><td>"+row_dato1_f[1]+" "+row_dato1_f[2]+"</td><td class=\"id_fecha_fecha\">"+row_dato1_f[3]+"</td><td class='subtotal_ped_fun_busq'>"+row_dato1_f[4]+"</td><td><img class='detalle_prod_fecha' src=\"../images/icon_svg/write13.svg\" width=\"20em\" height=\"20em\" title=\"Ver Detalles \"> <img class='eliminar_pedido_fun' src=\"../images/icon_svg/delete84.svg\" width=\"25em\" height=\"25em\" title=\"Eliminar Pedido\"></td></tr>");
                            break;
                        }
                    };*/

                    for (var i = 0; i < row_dato1.length; i++) {
                        var row_dato1_f=row_dato1[i].split("|");
                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                            //alert(row_dato1_f[0]);
                            $("#t_body_lpendientes_original").append("<tr id=\"pendiente-"+row_dato1_f[0]+"\"><td class=\"id_ped_fecha\">"+row_dato1_f[0]+"</td><td>"+row_dato1_f[1]+" "+row_dato1_f[2]+"</td><td class=\"id_fecha_fecha\">"+row_dato1_f[3]+"</td><td class='subtotal_ped_fun_busq pendiente-"+row_dato1_f[0]+"'>"+row_dato1_f[4]+"</td><td class='acciones'><img class=\"pagar\" src=\"../images/icon_svg/money175.svg\" width=\"25em\" height=\"25em\" title=\"Realizar Pago\">    <img class=\"detalle_ped\" src=\"../images/icon_svg/write13.svg\" width=\"25em\" height=\"25em\" title=\"Ver Detalles\"></td></tr>");
                            break;
                        }
                    };

                    calcula_total_fun_busq();
                });
            }//fin else
        }
    

    
        function pedidos_usuario(id,soli,fecha) {
            var x_id=id;
            var solicitud=soli;
            var fecha_id=fecha;

            if(solicitud=='ped'){
                $(".id_detal").html("#"+x_id+" - Fecha "+fecha_id);
                //alert(solicitud);

                $.post("get_ped_us.php",{user:x_id, bandera:'sin_fecha'},function(dato1,status){
                    row_dato1=dato1.split(";");

                    for (var i = 0; i < row_dato1.length; i++) {
                        var row_dato1_f=row_dato1[i].split("|");
                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                            $("#body_t_pedido_usuario").append("<tr><td class=\"id_ped_fun\">"+row_dato1_f[0]+"</td><td class=\"nombre\">"+row_dato1_f[1]+" "+row_dato1_f[2]+"</td><td class=\"id_fecha_fun\">"+row_dato1_f[3]+"</td><td class=\"subtotal_ped_fun_busq\">$"+parseFloat(row_dato1_f[4], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+"</td><td><img class=\"detalle_prod_fun\" src=\"../images/icon_svg/write13.svg\" width=\"20em\" height=\"20em\" title=\"Ver Detalles de Pedido\"> <img class='eliminar_pedido_fun' src=\"../images/icon_svg/delete84.svg\" width=\"20em\" height=\"20em\" title=\"Eliminar Pedido\"></td></tr>");
                            break;
                        }
                    };
                    calcula_total_fun_busq();
                });
            }

            if(solicitud=='prod'){
                $(".id_detal").html("#"+x_id+" - Fecha "+fecha_id);

                $.post("get_ped_us.php",{id_ped:x_id, bandera:'det_prod'},function(dato1,status){
                    row_dato1=dato1.split(";");

                    for (var i = 0; i < row_dato1.length; i++) {
                        var row_dato1_f=row_dato1[i].split("|");
                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                            $("#body_t_producto").append("<tr><td>"+row_dato1_f[0]+"</td><td id='calcula_total_ped'>"+row_dato1_f[1]+"</td><td>"+row_dato1_f[2]+"</td></tr>");
                            break;
                        }
                    };
                    calcula_total_fun_busq();
                });
            }
        }

        function registro_pagos(id) {
            $.post("reg_pag.php",{id_fun:id},function(datos){
                
                row_datos=datos.split(";");
                   
                for (var i = 0; i < row_datos.length; i++) {
                    var row_dato1_f=row_datos[i].split(",");
                    
                     for (var z = 0; z < row_dato1_f.length; z++) {
                        var row_dato1_f2=row_dato1_f[z].split("|");
                    
                        for (var x = 0; x < (row_dato1_f2.length-1); x++) {
                            $("#body_t_reg_pagos").append("<tr><td>"+(i+1)+"</td><td>"+row_dato1_f2[0]+"</td><td>"+row_dato1_f2[1]+"</td><td>"+row_dato1_f[1]+"</td></tr>");
                            //alert(row_dato1_f2[1]);
                            break;
                        }
                    };
                };
            });
        }
    