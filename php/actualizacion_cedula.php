<?php 
	include 'conn.php';
	date_default_timezone_set('America/Bogota');
	$fecha_pago	= date("Y-m-d");

	// $cedula = [ 'CedulaMala' => 'cedulaBuena' ]

	$cedula = [
		'1088' => '1088245234'
	];

	$cedulaB = [
		'1088245234' => '1088'
	];

	$actualizaciones = [];

	foreach ($cedula as $cedulaMala => $cedulaBuena) {

		$sqlCountFuncionariosCedulaMala 	= "SELECT COUNT(id_funcionario) as cantidadFuncionariosCedulaMala FROM `funcionarios` WHERE  `id_funcionario` = ".$cedulaMala;
		$queryFuncionariosCedulaMala 		= mysqli_query($conn, $sqlCountFuncionariosCedulaMala);
		$countFuncionariosCedulaMala 		= mysqli_fetch_array($queryFuncionariosCedulaMala,MYSQLI_BOTH);
		$actualizaciones['cantidadFuncionarios'][$cedulaMala]['cantidadFuncionariosCedulaMala'] = $countFuncionariosCedulaMala['cantidadFuncionariosCedulaMala'];

		if ($countFuncionariosCedulaMala['cantidadFuncionariosCedulaMala'] == 1) {

			$sqlCountFuncionariosCedulaBuena 		= "SELECT COUNT(id_funcionario) as cantidadFuncionariosCedulaBuena FROM `funcionarios` WHERE  `id_funcionario` = ".$cedulaBuena;
			$queryCountFuncionariosCedulaBuena 		= mysqli_query($conn, $sqlCountFuncionariosCedulaBuena);
			$countFuncionariosCedulaBuena 			= mysqli_fetch_array($queryCountFuncionariosCedulaBuena,MYSQLI_BOTH);
			$actualizaciones['cantidadFuncionarios'][$cedulaMala]['cantidadFuncionariosCedulaBuena'] = $countFuncionariosCedulaBuena['cantidadFuncionariosCedulaBuena'];

			if ($countFuncionariosCedulaBuena['cantidadFuncionariosCedulaBuena'] == 0) {

				$sqlUpdateFuncionario 				= "UPDATE funcionarios SET id_funcionario = ".$cedulaBuena." WHERE id_funcionario = ".$cedulaMala." ";
				$queryUpdateFuncionario 			= mysqli_query($conn, $sqlUpdateFuncionario);
				if ($queryUpdateFuncionario == true) {

					$sqlCountUpdatePagos		= "SELECT COUNT(id_funcionario) as cantidadPagos FROM `pagos` WHERE  `id_funcionario` = ".$cedulaMala." ";
					$queryCountUpdatePagos 		= mysqli_query($conn, $sqlCountUpdatePagos);
					$countUpdatePagos 			= mysqli_fetch_array($queryCountUpdatePagos,MYSQLI_BOTH);
					$actualizaciones['pagos'][$cedulaMala]['cantidadPagos'] = $countUpdatePagos['cantidadPagos'];

					if ($countUpdatePagos['cantidadPagos'] > 0) {

						$sqlUpdatePagos 			= "UPDATE pagos SET id_funcionario = ".$cedulaBuena." WHERE id_funcionario = ".$cedulaMala." ";
						$queryUpdatePagos 			= mysqli_query($conn, $sqlUpdatePagos);
						$actualizaciones['pagos'][$cedulaMala]['pagosActualizados'] = $queryUpdatePagos ;

						if ($queryUpdatePagos == true) {
							$sqlCountSaldos 		= "SELECT COUNT(funcionarios_id_funcionario) as cantidadSaldos FROM `t_saldo` WHERE `funcionarios_id_funcionario` = ".$cedulaMala." ";
							$queryCountSaldos 		= mysqli_query($conn, $sqlCountSaldos);
							$countSaldos 			= mysqli_fetch_array($queryCountSaldos,MYSQLI_BOTH);
							$actualizaciones['saldos'][$cedulaMala]['cantidadSaldos'] = $countSaldos['cantidadSaldos'];

							if ($countSaldos['cantidadSaldos'] > 0) {
								$sqlUpdateSaldos 	= "UPDATE t_saldo SET funcionarios_id_funcionario = ".$cedulaBuena." WHERE funcionarios_id_funcionario = ".$cedulaMala." ";
								$queryUpdateSaldos 	= mysqli_query($conn, $sqlUpdateSaldos);
								$actualizaciones['saldos'][$cedulaMala]['saldosActualizados'] = $queryUpdateSaldos ;

								if ($queryUpdateSaldos == true) {
									$sqlCountPedidos 		= "SELECT COUNT(id_funcionario) as cantidadPedidos FROM `pedidos` WHERE `id_funcionario` = ".$cedulaMala." ";
									$queryCountPedidos 		= mysqli_query($conn, $sqlCountPedidos);
									$countPedidos 			= mysqli_fetch_array($queryCountPedidos,MYSQLI_BOTH);
									$actualizaciones['pedidos'][$cedulaMala]['cantidadPedidos'] = $countPedidos['cantidadPedidos'];

									if ($countPedidos['cantidadPedidos'] > 0) {
										$sqlUpdatePedidos 	= "UPDATE pedidos SET id_funcionario = ".$cedulaBuena." WHERE id_funcionario = ".$cedulaMala." ";
										$queryUpdatePedidos 	= mysqli_query($conn, $sqlUpdatePedidos);
										$actualizaciones['pedidos'][$cedulaMala]['pedidosActualizados'] = $queryUpdatePedidos;

										if ($queryUpdatePedidos == true) {
											$sqlCountUpdateLog = "SELECT COUNT(id_funcionario) as  cantidadLogs FROM log_delete_producto WHERE id_funcionario = ".$cedulaMala." ";
											$queryCountLog 		= mysqli_query($conn, $sqlCountUpdateLog);
											$countLog			= mysqli_fetch_array($queryCountLog,MYSQLI_BOTH);
											$actualizaciones['logs'][$cedulaMala]['cantidadLogs'] = $countLog['cantidadLogs'];

											if ($countLog['cantidadLogs'] > 0) {
												$sqlUpdateLog = "UPDATE log_delete_producto SET id_funcionario = ".$cedulaBuena." WHERE id_funcionario = ".$cedulaMala." ";
												$queryUpdateLogs 	= mysqli_query($conn, $sqlUpdateLog);
												$actualizaciones['logs'][$cedulaMala]['LogsActualizados'] = $queryUpdateLogs;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	echo json_encode($actualizaciones);

	//$sql_saldos_actuales = mysqli_query($conn,$sql_saldos_actuales); 
	//$saldo = mysqli_fetch_array($sql_saldos_actuales);
	

?>
