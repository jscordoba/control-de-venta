﻿<?php
if (!isset($_SESSION["id_sesion"])){
    header("Location:index.php");
}else{
    function enviar_correo($id_user,$valor,$email,$pedido, $opcion = 'insert'){
    	date_default_timezone_set('America/Bogota');
    	include 'conn.php';
    	require_once '../lib/PHPMailer/class.phpmailer.php' ;
		require_once '../lib/PHPMailer/class.smtp.php' ;

    	$msg = null;

		$mail = new PHPMailer;
		$mail->isSMTP();
		$mail->SMTPAuth = true;

		$mail->Host = "mail.office365.com";
		$mail->Port = 587;
		$mail->SMTPSecure = "tls";
		$mail->Username = "jenniferalzate@magnetron.com.co";
		$mail->Password = "Magnetron2018*";
		$mail->setFrom('jenniferalzate@magnetron.com.co');

		//fin configuracion

		$tipo = ($opcion != 'insert') ? 'Pedido eliminado' : '';
		$titulo_eliminacion = ($opcion != 'insert') ? 'Pedido Eliminado exitosamente' : '';

		$mail->FromName = utf8_decode("Administración CDMAG".$tipo);

		$mail->IsHTML(true);

		/* ##################################------------------########################################*/

		if (isset($conn)) {
			$mensaje_variable = ($opcion == 'insert') ? 'Se acaba de registrar una compra por valor de <b>'.$valor.' </b> y fue cargado a la cuenta con número de identificación <b>'.$id_user.'</b>.' : 'Se acaba de eliminar el registro de compra por valor de <b>'.$valor.' </b> que fue cargado a la cuenta con número de identificación <b>'.$id_user.'</b>.';
			$msg2='
			<div class="" style="width:80%; margin-left:10%">
				<h1 style="text-align: center; color: rgba(16, 80, 148,.9)">Notificación - CDMAG</h1>
				<h2 style="text-align: center; color: rgba(16, 80, 148,.9)">Un saludo de parte del fondo de empleados FodeMag</h2><br>
				<div class="" style="width: 100%; display: flex; justify-content: center; align-items: center;">
					<img data-imagetype="External" data-imageerror="RelWithoutBase" src="http://portal.magnetron.com:85/cdmag/images/icono-cafe.png" align="left" style="padding: .6em ;width:150px; height:150px; border-radius: 50%; box-shadow: 1px 1px 1px rgba(16, 80, 148,.6); margin-left: 43%;">
				</div>
				<div style="width: 100%; text-align: center;">
					<h4 style="color: rgb(245, 80, 32)">'.$titulo_eliminacion.'</h4>
				</div>
				<div style="width: 100%; ">
					<br>
					<p style="text-align: center ; color :rgba(16, 80, 148,.9);">'.$mensaje_variable.'</p> 
					<p></p>
				</div>
				
				<div class="" style="width:80%; margin-left: 10%">
					<table class="x_table" border="0" cellspacing="15" style="width:100%">
						<tbody>
							<tr>
								<td colspan="4" style="text-align:center; border-radius: 6px">
									<h3 style="text-align: center; background:rgba(16, 80, 148,.9); color: white; padding: .2em; margin-top: 1em; font-size: 1.2em">Detalles de la Compra</h3>
								</td>
								</tr>
							<tr >
								<th style="text-align:center ; border-bottom: 1px solid rgba(16, 80, 148,.9); color: rgba(16, 80, 148,1); padding: .1em"><b>Producto</b></th>
								<th style="text-align:center ; border-bottom: 1px solid rgba(16, 80, 148,.9); color: rgba(16, 80, 148,1); padding: .1em"><b>Precio</b></th>
								<th style="text-align:center ; border-bottom: 1px solid rgba(16, 80, 148,.9); color: rgba(16, 80, 148,1); padding: .1em"><b>Cantidad</b></th>
								<th style="text-align:center ; border-bottom: 1px solid rgba(16, 80, 148,.9); color: rgba(16, 80, 148,1); padding: .1em">Subtotal</th>
							</tr>';

						if ($opcion == 'insert') {
							$sql="SELECT pr.descripcion, pp.precio_unidad, pp.cantidad
			                    FROM productos pr LEFT JOIN pedido_producto pp on(pr.id_producto=pp.id_producto)
			                    LEFT JOIN pedidos p on(pp.id_pedido=p.id_pedido)
			                    LEFT JOIN funcionarios f ON (p.id_funcionario=f.id_funcionario)
			                    WHERE p.id_pedido='$pedido'";
						}else{
							 $sql = "SELECT pr.descripcion, lp.precio_unidad, lp.cantidad
                                FROM productos pr LEFT JOIN log_delete_producto lp on(pr.id_producto = lp.id_producto)
                                LEFT JOIN funcionarios f ON (lp.id_funcionario=f.id_funcionario)
                                WHERE lp.id_pedido='$pedido'";
						}
						
						//print_r($sql);

						$result=mysqli_query($conn,$sql);
						
						$msg3="";
						while ($detalle=mysqli_fetch_array($result,MYSQLI_BOTH)) {
							$msg3.= '<tr>
										<td style="text-align:center">'.$detalle[0].'</td>
										<td style="text-align:center">'.$detalle[1].'</td>
										<td style="text-align:center">'.$detalle[2].'</td>
										<td style="text-align:center">'.$detalle[1]*$detalle[2].'</td>
									</tr>';
						}

					$msg4 = '</tbody>
						</table>
						<div class="footer" style="text-align: center; color: rgba(16, 80, 148,.9); border: 1px solid rgba(16, 80, 148,.9); margin-top: 2em ; padding: .5em; font-size: 1.2em; border-radius: 6px">
							<p>Favor siempre tenga en cuenta esta información para cualquier tipo de solicitud con respecto a las compras realizadas.</p>
							<p>Muchas gracias por usar nuestro servicio.<br>
							Feliz día.
							</p>
						</div>
						<p style="text-align: center; width: 100%; color:rgba(16, 80, 148,.9)">Copyright © 2015 Magnetron S.A. All rights reserved - TIC\'s </p>
					</div>
				</div>';

			$asunto = utf8_decode("Notificación de Pedido #".$pedido." - CDMAG");
			$mensaje = utf8_decode($msg2.$msg3.$msg4);
    	}else{
    		echo "Sin Conexión a la DB";
    	}

    	/* ##################################------------------########################################*/

    	$mail->Subject = $asunto;

		$mail->addAddress($email, $id_user);

		$mail->MsgHTML($mensaje);


		if (isset($adjunto)) {
			$adjunto = $_FILES["adjunto"];

			if ($adjunto ["size"] > 0)
			{
			   
				$mail->addAttachment($adjunto ["tmp_name"], $adjunto ["name"]);
			}
		}

		if($mail->Send())
		{
			$msg= "ok";
			echo "siiiii";
			echo $email;
		}
		else
		{
			$msg = "Lo siento, ha habido un error al enviar el mensaje a $email";
		}

    }
}