<?php session_start();
if (!isset($_SESSION["id_sesion"])){
    header("Location:index.php");
}else{
    include 'conn.php';
    date_default_timezone_set('America/Bogota');
    if ($_REQUEST) {
        $bandera=$_REQUEST['bandera'];

        if($bandera=='con_fecha'){
            $f_ini=$_REQUEST['fecha_ini'];
            $f_fin=$_REQUEST['fecha_fin'];
            $id_user=$_REQUEST['user'];

            //MUESTRA EL SALDO A DEBER EN RANGO DE FECHA PARA UNA PERSONA ESPECIFICA
            /*$query=mysqli_query($conn,"SELECT f.id_funcionario, f.nombres,f.apellidos,t_saldo.t_saldo_fecha,t_saldo.t_saldo_valor
                                            FROM t_saldo
                                                JOIN funcionarios f ON(t_saldo.funcionarios_id_funcionario=f.id_funcionario)
                                            WHERE t_saldo.idt_saldo IN(SELECT max(ts.idt_saldo) as idt
                                                                        FROM t_saldo ts 
                                                                        WHERE ts.t_saldo_fecha BETWEEN '$f_ini' AND '$f_fin'
                                                                        AND ts.funcionarios_id_funcionario='$id_user'
                                                                        GROUP BY ts.funcionarios_id_funcionario)
                                            AND t_saldo.t_saldo_valor>0");*/

            $query=mysqli_query($conn,"SELECT  f.id_funcionario, f.nombres, f.apellidos, T.t_saldo_fecha, T.t_saldo_valor
                                        FROM    funcionarios f
                                        INNER JOIN t_saldo T ON T.funcionarios_id_funcionario = f.id_funcionario
                                        WHERE   T.idt_saldo = ( 
                                                                SELECT  MAX( ts.idt_saldo ) AS idt
                                                                FROM    t_saldo ts
                                                                WHERE   ts.t_saldo_fecha
                                                                BETWEEN '$f_ini' AND '$f_fin'
                                                        AND ts.funcionarios_id_funcionario = $id_user
                                                    )
                                        AND T.t_saldo_valor >0 
                                        ORDER BY `f`.`nombres` ASC");


            while ( $row=mysqli_fetch_array($query,MYSQLI_BOTH)) {
                if ($row[4]==0 || $row[4]=="0") {
                    
                }else{
                    echo ($row[0]."|".$row[1]."|".$row[2]."|".$row[3]."|".$row[4].";");
                }
            }
        }elseif($bandera=='con_fecha_rango'){
            $f_ini=$_REQUEST['fecha_ini'];
            $f_fin=$_REQUEST['fecha_fin'];
            $id_user=$_REQUEST['user'];

            /*if ($f_ini=="") {
                
            }*/

            //MUESTRA EL SALDO A DEBER EN RANGO DE FECHA GENERAL
            $query=mysqli_query($conn,"SELECT  f.id_funcionario, f.nombres, f.apellidos, T.t_saldo_fecha, T.t_saldo_valor
                                        FROM    funcionarios f
                                        INNER JOIN t_saldo T ON T.funcionarios_id_funcionario = f.id_funcionario
                                        WHERE   T.idt_saldo = ( 
                                                                SELECT  MAX( ts.idt_saldo ) AS idt
                                                                FROM    t_saldo ts
                                                                WHERE   ts.t_saldo_fecha
                                                                BETWEEN '$f_ini' AND '$f_fin'
                                                                AND ts.funcionarios_id_funcionario = f.id_funcionario
                                                                )
                                        AND T.t_saldo_valor >0 
                                        ORDER BY `f`.`nombres` ASC");

            while ( $row=mysqli_fetch_array($query,MYSQLI_BOTH)) {
                echo ($row[0]."|".$row[1]."|".$row[2]."|".$row[3]."|".$row[4].";");
            }
        }elseif($bandera=='sin_fecha'){
            $id_user=$_REQUEST['user'];

            $query=mysqli_query($conn,"SELECT p.id_pedido,f.nombres,f.apellidos,p.fecha,p.valor
                                        FROM pedidos p JOIN funcionarios f ON (f.id_funcionario=p.id_funcionario)
                                        WHERE f.id_funcionario=$id_user
                                        ORDER BY p.id_pedido DESC
                                        LIMIT 35");

            while ( $row=mysqli_fetch_array($query,MYSQLI_BOTH)) {
                echo ($row[0]."|".$row[1]."|".$row[2]."|".$row[3]."|".$row[4].";");
            }
        }elseif($bandera=='det_prod'){
            $id_pedi=$_REQUEST['id_ped'];

            $query=mysqli_query($conn,"SELECT pr.descripcion, pp.precio_unidad, pp.cantidad
                                        FROM productos pr LEFT JOIN pedido_producto pp on(pr.id_producto=pp.id_producto)
                                        LEFT JOIN pedidos p on(pp.id_pedido=p.id_pedido)
                                        LEFT JOIN funcionarios f ON (p.id_funcionario=f.id_funcionario)
                                        WHERE p.id_pedido=$id_pedi");

            while ( $row=mysqli_fetch_array($query,MYSQLI_BOTH)) {
                echo ($row[0]."|".$row[1]."|".$row[2].";");
            }
        }elseif($bandera=='info_prod'){
            //$id_pedi=$_REQUEST['id_ped'];

            $query=mysqli_query($conn,"SELECT p.id_pedido,pr.descripcion, pp.precio_unidad, pp.cantidad,p.id_funcionario,f.nombres,f.apellidos
                                        FROM productos pr RIGHT JOIN pedido_producto pp on(pr.id_producto=pp.id_producto)
                                        JOIN pedidos p on(pp.id_pedido=p.id_pedido)
                                        JOIN funcionarios f ON (p.id_funcionario=f.id_funcionario)");
        }
    }
}

?>