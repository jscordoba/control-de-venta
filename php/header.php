<?php
include 'styles.php';
 $url = $_SERVER['REQUEST_URI'];
?>

<head>
    <meta charset="UTF-8">
     <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
     <link rel="stylesheet" href="../css/style.css">
</head>

<nav class="navbar navbar-default">
   <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
         <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         </button>
         <a class="navbar-brand" href="pedidos.php"><img alt="Brand" src="../images/magnetron.jpg" class="" style="width:3em;height:3em; margin-top: -19px"></a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
            <?php if (isset($_SESSION['id_sesion'])) { ?>
                <li ><a href="#" class="viewSession"><?= $_SESSION["icon"] ?> <?= $_SESSION["id_sesion"]; ?></a></li>
                <li class="<?= ($url == "/cdmag/php/ventas_dia.php?pl=all" || $url == "/cdmag/php/ventas_dia.php?pl=pl2" || $url == "/cdmag/php/ventas_dia.php?pl=pl1" ) ? 'active' : ''  ?>">
                    <a href="ventas_dia.php?pl=<?= $_SESSION['rol_venta'] ?>"><i class="fas fa-shopping-basket right1"></i><span>Ventas del dia</span></a>
                </li>
                <?php if ($_SESSION['id_sesion'] =='venta' || $_SESSION['id_sesion'] == 'ventasp2' || $_SESSION['id_sesion'] == 'admin'  ) { ?>
                    <li class="<?= ($_SERVER['REQUEST_URI'] == "/cdmag/php/query.php") ? 'active' : ''  ?>">
                        <a href="query.php"><i class="fas fa-cart-plus right1"></i><span>Ingresar Pedidos</span></a>
                    </li>
                <?php } ?>
                <li class="<?= ($_SERVER['REQUEST_URI'] == "/cdmag/php/ing_prod.php") ? 'active' : ''  ?>">
                    <a href="ing_prod.php"><i class="fas fa-apple-alt right1"></i><span>Productos</span></a>
                </li>
                 <li class="<?= ($_SERVER['REQUEST_URI'] == "/cdmag/php/insertar_usuario.php") ? 'active' : ''  ?>">
                    <a href="insertar_usuario.php"><i class="fas fa-user-plus"></i></i> <span>Crear funcionario</span></a>
                </li>
            <?php } ?>
            
            <?php if ($_SESSION['id_sesion']=='fodemag' || $_SESSION['id_sesion'] == 'admin' ) { ?>
                <li class="<?= ($_SERVER['REQUEST_URI'] == "/cdmag/php/ing_pag.php") ? 'active' : ''  ?>">
                    <a href="ing_pag.php"><i class="fas fa-balance-scale right1"></i> <span>Movimientos</span></a>
                </li>
                <li class="<?= ($_SERVER['REQUEST_URI'] == "/cdmag/php/ing_prod.php") ? 'active' : ''  ?>">
                    <a href="ing_prod.php"><i class="fas fa-apple-alt right1"></i><span>Productos</span></a>
                </li>
                <li class="<?= ($_SERVER['REQUEST_URI'] == "/cdmag/php/ing_func.php") ? 'active' : ''  ?>">
                    <a href="ing_func.php"><i class="fas fa-users right1"></i><span>Funcionarios</span></a>
                </li>
                <li class="<?= ($_SERVER['REQUEST_URI'] == "/cdmag/php/usuarios.php") ? 'active' : ''  ?>">
                    <a href="usuarios.php"><i class="fas fa-users-cog right1"></i> <span>Usuarios</span></a>
                </li>
                <li class="<?= ($_SERVER['REQUEST_URI'] == "/cdmag/php/Reportes.php") ? 'active' : ''  ?>">
                    <a href="Reportes.php"><i class="fas fa-file-archive right1"></i><span>Reportes</span></a>
                </li>
            <?php } ?>
         </ul>
         <ul class="nav navbar-nav navbar-right ">
            <li><a href="logout.php" class="bg-red" ><i class="fas fa-power-off "></i></a></li>
         </ul>
      </div><!-- /.navbar-collapse -->
   </div><!-- /.container-fluid -->
</nav>