<style>
    ul.pagination{
        position: fixed;
        bottom: -20px;
        background: #101010;
        width: 100%;
        height: 4em;
        justify-content: center;
        display: flex;
        align-items: center;
    }
</style>
<?php session_start(); 
date_default_timezone_set('America/Bogota');
$q = (isset($_POST['q']) ? "'%".$_POST['q']."%'" : "'%%'" ) ;
$estado = (isset($_POST['estado']) ? $_POST['estado'] : 1 ) ;
$estado = ($estado == 2) ? "'%%'" : $estado ;
$pagina = (isset($_GET['pag']) ? $_GET['pag'] : 1 ) ;
$limit = 50;
$offset =($limit * $pagina) - $limit;

if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}elseif ($_SESSION['id_sesion']=='fodemag' OR $_SESSION['id_sesion']=='admin') {
	
	include 'header.php';
    include 'conn.php';
?>

    <script>
        $(document).ready(function(){
            $(".nuevo_fun").live('click',function(){
                $("#lista").css("display", "none");
                $("#filtroBusqueda").css("display", "none");
                $("ul.pagination").css("display", "none");
                $("#registro").css("display", "block");
                $(".volver_ing_func").css("display", "block");
            });

            $(".registrar_fun").live('click',function(){
                var c_id_fun=$("#id_func").val();
                var c_nombres=$("#nombres").val();
                var c_apellidos=$("#apellidos").val();
                var c_area=$(".list_areas").val();
                var c_email=$("#email").val();

                //alert(c_id_fun+c_nombres+c_apellidos+c_area+c_email);

                $.post("ins_fun.php",{id_fun:c_id_fun, nombres:c_nombres, apellidos:c_apellidos, area:c_area, email:c_email, opc:'ins'}, function(date){
                    alert(date);

                    $("#registro").css("display", "none");
                    $("#lista").css("display", "block");
                });
                location.reload();
            });

            $(".eliminar_fun").live('click',function(){
                var c_id_fun=$(this).attr('identificador');
                var x=confirm("Favor validar que no tenga cuentas pendientes.\nDesea eliminar el funcionario seleccionado?\n"+c_id_fun);

                if(x==true){
                    $.post("ins_fun.php",{id_fun:c_id_fun, opc:'del'}, function(data){
                        alert(data);
                        //alert("Funcionario Eliminado");
                    });
                    location.reload();
                }
                else{}
            });
            $(".activar_fun").live('click',function(){
                var c_id_fun = $(this).attr('identificador');
                console.log(c_id_fun);
                $.post("ins_fun.php",{id_fun:c_id_fun, opc:'activar'}, function(data){
                    alert(data);
                });
                location.reload(); 
            });
        });
    </script>

    <script>
    function get_fun_nom(und) {
        var c_id_fun=$("#id_func").val();
        if(und==""){
            $(".nombres").attr("disabled",true);
        }else if (und=="NaN"){
            $(".nombres").attr("disabled",true);
        }else{
            $.post("ins_fun.php",{id_fun:c_id_fun, opc:'validar'},function(dato1){
                if (dato1) {
                       alert("Ya existe");
                       $(".nombres").attr("disabled",true); 
                }else{
                    $(".nombres").attr("disabled",false); 
                }
            });
        }
    }
    function get_fun_ape(und1) {

        if(und1==""){
            $(".apellidos").attr("disabled",true);
        }else if (und1=="NaN"){
            $(".apellidos").attr("disabled",true);
        }else{
            $(".apellidos").attr("disabled",false);
        }
    }
    
    function get_fun_registrar(und2) {

        if(und2==""){
            $(".registrar_fun").attr("disabled",true);
        }else if (und2=="NaN"){
            $(".registrar_fun").attr("disabled",true);
        }else{
            $(".registrar_fun").attr("disabled",false);
        }
    }
    </script>
    <div class="row" id="filtroBusqueda">
        <form action="ing_func.php" method="post">
            <div class="col-xs-12 col-sm-2 col-sm-offset-6">
                <div class="form-group">
                    <select class="form-control" name="estado">
                        <option value="" disabled="" selected="" >Estado</option>
                        <option <?= ($estado == 1) ? 'selected' : '' ?> value="1">Activo</option>
                        <option <?= ($estado == 0) ? 'selected' : '' ?> value="0">Inactivo</option>
                        <option <?= (isset($_POST['estado']) && $_POST['estado'] == 2) ? 'selected' : '' ?> value="2">Todos</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3">
                <div class="input-group">
                    <input autocomplete="off" type="text" value="<?= isset($_POST['q']) ? $_POST['q'] : ''?>" name="q" class="form-control" placeholder="Cedula o nombre o apellido">
                    <span class="input-group-btn">
                        <button class="btn btn-success" type="submit">Buscar</button>
                    </span>
                </div>
            </div>
         </form>
    </div>
    
    <div class="row top1" id="registro" style="display: none; text-align:center;">

        <div class="col-xs-2 col-md-2"></div>
        <div class="col-xs-8 col-md-8" style="text-align:center;">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><h4>Registrar Nuevo Funcionario</h4></div>

                <form class="form-horizontal" style="margin:2em;">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Identificación</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" class="form-control id_fun_nuevo" autofocus="" id="id_func" autocomplete="off" placeholder="Número de identificación" onkeyup="get_fun_nom(this.value)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label" disabled>Nombres</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" class="form-control nombres" id="nombres" placeholder="Nombres" disabled onkeyup="get_fun_ape(this.value)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Appelidos</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" class="form-control apellidos" id="apellidos" placeholder="Appelidos" disabled onkeyup="get_fun_registrar(this.value)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Área</label>
                        <div class="col-sm-10 col-md-8">
                            <?php
                                $result=mysqli_query($conn, "SELECT * FROM funcionarios WHERE estado = true AND nombres LIKE $q OR id_funcionario LIKE $q GROUP BY area ORDER BY area DESC");
                                $num_areas=mysqli_num_rows($result);
                                echo "<select size='1' class=\"form-control list_areas\" name=\"list_areas\">";
                                $i=0;
                                while ($areas=mysqli_fetch_array($result)) {
                                  echo "<option value='".$areas['area']."'>".$areas['area']."</option>";
                                  $i++;
                                }
                              ?>
                              <option value="" selected>Seleccione...</option>
                              </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Correo</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="email" class="form-control email" id="email" placeholder="Correo Electronico" >
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default registrar_fun" disabled>Registrar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-xs-2 col-md-2 col-lg-2 col-sm-3 volver_ing_func"><a href="ing_func.php"><button type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>
        </div>
        <div class="col-xs-1"></div>
    </div>

	<div class="row top1" id="lista" style="display: block; text-align: center">
		<div class="col-xs-1"> </div>
            <div class="col-xs-10">
                <div class="panel panel-default" style="text-align: center">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><h4>Lista de Funcionarios</h4></div>

                    <!-- Table -->
                    <table class="table table-striped" style="width: 100%; text-align: center">
                       <!-- <table class="table table-striped">-->
                        <tr>
                            <td><b>Estado</b></td>
                            <td><b>Identificacion</b></td>
                            <td><b>Nombre</b></td>
                            <td><b>Área</b></td>
                            <td><b>Correo</b></td>
                            <td><b>Acciones</b> <img class="nuevo_fun" src="../images/icon_svg/add182.svg" width="25em" height="25em" title="Nuevo Funcionario">
                        </tr>
                        <tbody id="body_t_productos"></tbody>
                        <?php

                        $count=mysqli_query($conn,"SELECT COUNT(id_funcionario) AS cantidad FROM funcionarios WHERE  (nombres LIKE $q OR id_funcionario LIKE $q OR apellidos LIKE $q) AND estado = ".$estado." ");
                        $count = $count->fetch_array(MYSQLI_NUM);
                        $cantidadFuncionarios = $count[0];
                        $numPaginas = ceil($count[0] / $limit);
                       
                        $sql = "SELECT * FROM funcionarios WHERE (id_funcionario LIKE $q  OR nombres LIKE $q OR apellidos LIKE $q) AND estado LIKE ".$estado."  ORDER BY nombres ASC LIMIT $limit offset $offset";
                        $query=mysqli_query($conn, $sql);
                        while($row=mysqli_fetch_array($query,MYSQLI_BOTH)){ ?>
                            <tr>
                                <td>
                                    <?= ($row[5] == 0) ? "<i class='fas cursor size2 fa-certificate red eliminar_fun' title='Usuario inactivo'></i>" : " <i class='fas cursor size2 fa-certificate green activar_fun' title='Usuario activo'></i>" ; ?>
                                </td>
                                <td class="c_id_func"><?= $row[0] ?></td>
                                <td> <?= $row[1]." ".$row[2] ?> </td>
                                <td> <?= $row[3] ?> </td>
                                <td> <?= $row[4] ?> </td>
                                <td>
                                    <a href="insertar_usuario.php?id=<?= $row[0] ?>"><i class="fas fa-user-edit green right1"></i></a>
                                    <?= ($row[5] == 1) ? "<i class='fas cursor size2 fa-trash-alt red eliminar_fun' title='Eliminar Funcionario' identificador=".$row[0]."></i>" : " <i class='fas cursor size2 fa-check-square green activar_fun' title='Activar Funcionario' identificador=".$row[0]."></i>" ; ?>
                                   
                                </td>
                            </tr>
                         <?php } ?>
                    </table>
                </div>
            <div class="col-xs-2 col-md-2 col-lg-2 col-sm-3"><a href="pedidos.php"><button type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>
            </div>
		</div>
		<div class="col-xs-1"></div>
	</div>

    <ul class="pagination">
        <?php for ($i = 1 ; $i <= $numPaginas ; $i++) {  ?>
            <li><a href="ing_func.php?pag=<?= $i ?>" class="paginacion <?= ($_GET['pag'] == $i) ? 'bg-green' : '' ?> " pag="<?= $i ?>"><?= $i ?></a></li>
        <?php } ?>
    </ul>
	

<?php	include 'footer.php';
}
else{
  header("Location:pedidos.php"); 
}
?>

