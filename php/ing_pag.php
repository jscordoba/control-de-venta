<?php 
session_start();

if (!isset($_SESSION["id_sesion"])){
    header("Location:index.php");
}elseif ($_SESSION['id_sesion']=='fodemag' OR $_SESSION['id_sesion']=='admin') {
    include 'header.php';
    include 'conn.php';
    date_default_timezone_set('America/Bogota');
    $f_ini=date("Y-m-d");
    $f_fin=date("Y-m-d");

    ?>

    <script>
        $(document).ready(function(){
        	
        calcula_total_fun();

        $(".volver_ing_pag").css("display","none");
        $(".volver_ing_pag_2").css("display","none");
        $(".reg_volver_ing").css("display","none");

            $(".pagar").live('click',function(){
                var pag_debe=$(this).parent().parent().children().next().next().next().html();
                var id_funcionario=$(this).parent().parent().children().html();
                var pag_nombre=$(this).parent().parent().children().next().html();

                //alert(pag_debe+"-"+id_funcionario);

                //SE ENVIA PARA VALIDAR VALOR REAL DE DEUDA Y MOSTRARLA "EN CASO DE TENER UNA, SI NO, NO PERMITE REALIZAR PAGOS."
                $.post("ins_pag.php",{op:'validar_pago', id_fun: id_funcionario},function(data){
                    //alert(data);

                    if (data!="NOPAY") {
                        //alert(data);
                        $("#lista").css("display", "none");
                        $("#registro").css("display", "none");
                        $("#pagos").css("display", "block");
                        $(".reg_volver_ing_pag").css("display","block");
                        $("#lista_pago_func_busq").css("display", "none");

                        $(".pag_cedula").html(id_funcionario);
                        $(".pag_nombres").html(pag_nombre);
                        $(".pag_debe").html(data);
                    }else{
                        alert(data);
                    }
                });
            });

             $(".pago-atras").live('click',function(){
                var pag_debe=$(this).parent().parent().children().next().next().next().html();
                var id_funcionario=$(this).parent().parent().children().html();
                var pag_nombre=$(this).parent().parent().children().next().html();

                //alert(pag_debe+"-"+id_funcionario);

                //SE ENVIA PARA VALIDAR VALOR REAL DE DEUDA Y MOSTRARLA "EN CASO DE TENER UNA, SI NO, NO PERMITE REALIZAR PAGOS."
               
                //alert(data);
                $("#lista").css("display", "block");
                $("#registro").css("display", "none");
                $("#pagos").css("display", "none");
                $(".reg_volver_ing_pag").css("display","none");
                $("#lista_pago_func_busq").css("display", "block");

                $(".pag_cedula").html(id_funcionario);
                $(".pag_nombres").html(pag_nombre);
                   
            });

            $(".registrar_pago").live('click',function(){ //DATOS PARA REALIAR PAGO
                var c_id_fun=$(".pag_cedula").html();
                var c_valor_pag=parseInt($(".pag_valor").val());
                var c_observacion_pag=$(".pag_observacion").val();
                var c_valor_debe=parseInt($(".pag_debe").html());

                if (c_valor_pag<=c_valor_debe) {
                    var resultado=c_valor_debe - c_valor_pag;

                    $.post("ins_pag.php",{id_funcionario:c_id_fun, restante: resultado, valor_pag:c_valor_pag, observacion_pag:c_observacion_pag},function(){
                        location.reload();

                        $("#registro").css("display", "none");
                        $("#lista").css("display", "block");
                        $("#pagos").css("display", "none");
                        $("#lista_pago_func_busq").css("display", "none");
                    });
                }else{
                    alert("El valor a pagar no es valido");
                }
            }); 

            $(".detalle_ped").live('click',function(){
                $("#lista").css("display", "none");
                $("#registro").css("display", "none");
                $("#pagos").css("display", "none");
                $("#lista_pago_func_busq").css("display", "none");
                $("#lista_por_funcionario").css("display", "block");

                var id=$(this).parent().parent().children().html();
                var fecha_id_ped=$(this).parent().parent().children().next().next().next().html();
                var tipo='ped';

                //alert("Id: "+id+" Tipo: "+tipo+" Fecha: "+fecha_id_ped);
                pedidos_usuario(id,tipo,fecha_id_ped);
                registro_pagos(id);
                
                $(".volver_ped").css("display", "none");
                $(".volver_ing_pag").css("display", "block");
            });

            $(".detalle_prod_fun").live('click',function(){

                var id_prod= $(this).parent().parent().children().html();
                var fecha_id_prod=$(this).parent().parent().children().next().next().html();
                var tipo='prod';

                $(".volver_ped").css("display", "none");
                $(".volver_ing_pag").css("display", "none");
                $("#lista_por_funcionario").css("display", "none");
                $("#t_detalles_prod").css("display", "none");
                
                //alert("Id: "+id_prod+" tipo: "+tipo+" fecha: "+fecha_id_prod);

                pedidos_usuario(id_prod,tipo,fecha_id_prod);

                $("#lista").css("display", "none");
                $("#registro").css("display", "none");
                $("#pagos").css("display", "none");
                $("#form_print").css("display", "none");
                $("#lista_pago_func_busq").css("display", "none");
                $("#t_detalles_prod").css("display", "block");
                $(".volver_ing_pag_2").css("display", "block");
            });

            $(".detalle-atras").live('click',function(){

                var id_prod= $(this).parent().parent().children().html();
                var fecha_id_prod=$(this).parent().parent().children().next().next().html();
                var tipo='prod';

                $(".volver_ped").css("display", "none");
                $(".volver_ing_pag").css("display", "block");
                $("#lista_por_funcionario").css("display", "block");
                
                //alert("Id: "+id_prod+" tipo: "+tipo+" fecha: "+fecha_id_prod);

                //pedidos_usuario(id_prod,tipo,fecha_id_prod);
                $('#body_t_producto td').remove();

                $("#lista").css("display", "block");
                $("#registro").css("display", "block");
                $("#pagos").css("display", "none");
                $("#form_print").css("display", "none");
                $("#lista_pago_func_busq").css("display", "none");
                $("#t_detalles_prod").css("display", "none");
                $(".volver_ing_pag_2").css("display", "none");
            });

            $(".detalle_prod_fecha").live('click',function(){
                $(".volver_ped").css("display", "none");
                $(".volver_ing_pag").css("display", "block");
                $("#lista_por_funcionario").css("display", "none");
                $("#t_detalles_prod").css("display", "none");

                var id_prod_b=$(this).parent().parent().children().html();
                var fecha_id_prod_busq=$(this).parent().parent().children().next().next().html();
                var tipo='prod';

                //alert("Id: "+id_prod_b+" tipo: "+tipo+" fecha: "+fecha_id_prod_busq);

                pedidos_usuario(id_prod_b,tipo,fecha_id_prod_busq);

                $("#lista").css("display", "none");
                $("#registro").css("display", "none");
                $("#pagos").css("display", "none");
                $("#form_print").css("display", "none");
                $("#lista_pago_func_busq").css("display", "none");
                $("#t_detalles_prod").css("display", "block");
            });

            $("#cargaArchivo").on('change', function(event) {
                event.preventDefault();
                var d = new Date();
                console.log(d.getHours()+"-"+d.getMinutes()+"-"+d.getSeconds());
                var formData = new FormData(document.getElementById("uploadCargaMasiva"));
                $.ajax({
                    beforeSend: function( xhr ) {
                        $('#messageUpload').text('El archivo se esta cargando...');
                        $("#iconSubir").removeClass('fas fa-file-upload').addClass('fa fa-spinner fa-spin');
                    },
                    error: function(XMLHttpRequest, textStatus, errorThrown) {
                        $("#iconSubir").css('color', 'red');
                        $('#messageUpload').text('Problemas con la red');
                    },
                    url: "rake.php",
                    type: "post",
                    dataType: "html",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false
                })
                .done(function(res){
                    var datos = JSON.parse(res);
                    var resp = datos['return'];
                    if (datos['updates'][0] != 0 || datos['updates'][0] != false ) {
                        var update = datos['updates'];
                        $.each( update, function( key, value ) {
                             $.each( value, function( llave, valor ) {
                                pagoExitoso = valor[0];
                                id_exitoso = llave;
                                console.log([pagoExitoso,id_exitoso]);
                                $('tr#pendiente-'+id_exitoso+" td").css('background-color', 'rgba(31,105,27,.8)');
                                $('tr#pendiente-'+id_exitoso+" td").css('color', 'white');
                                $('td.pendiente-'+id_exitoso).text("(" + $('td.pendiente-'+id_exitoso).text()+ " - " + pagoExitoso + ")" );
                            });
                        });
                    }
                    $("#iconSubir").removeClass('fa fa-spinner fa-spin').addClass('fas fa-file-upload');
                    if (resp[0] == 1 && resp[1] == 1) {
                        $("#iconSubir").css('color', 'green');
                        $('#messageUpload').text('Proceso exitoso');
                    }else if(resp[0] == 1 && resp[1] == 0){
                        $("#iconSubir").css('color', 'yellow');
                        $('#messageUpload').text('Fallo actualizacion saldos');
                    }else if (resp[0] == 0 && resp[1] == 1) {
                        $("#iconSubir").css('color', 'red');
                        $('#messageUpload').text('Fallo registro de pagos');
                    }else{
                        $("#iconSubir").css('color', 'red');
                        $('#messageUpload').text('El proceso no fue exitoso');
                    }
                });
                
            });

            $(".print_deben_fun").live('click',function(){
                $("#lista").css("display", "none");
                $("#registro").css("display", "none");
                $("#pagos").css("display", "none");
                $("#lista_pago_func_busq").css("display", "none");
                $(".volver_ing_pag").css("display","none");

                $("#form_print").css("display", "block");
                $("#form_print2").css("display", "block");
            });

            $(".print_pagos_fun").live('click',function(){
                $("#lista").css("display", "none");
                $("#registro").css("display", "none");
                $("#pagos").css("display", "none");
                $("#lista_pago_func_busq").css("display", "none");
                $("#lista_por_funcionario").css("display", "none");
                $(".volver_ing_pag").css("display","none");
                $("#form_print").css("display", "block");
            });
        });
    </script>

    <script language="javascript">
        
        function get_valor_pagar(und){ //VALIDAR VALOR RECIBIDO Y HABILITAR BOTON REGISTRAR PAGO
            //alert(und);
            var patron = /^\d*$/; //Expresión regular para aceptar solo números enteros

            if(und==""){
                $(".registrar_pago").attr("disabled",true);
            }else if(!patron.test(und)){
                $(".registrar_pago").attr("disabled",true);
            }else{
                $(".registrar_pago").attr("disabled",false);
            }
        }

    </script>

    <script type="text/javascript">
        $(".eliminar_pedido_fun").live('click',function(){//eliminar el pedido de un funcionario y descontar de las tablas FUNCIONARIOS_DEBEN, PEDIDOS, PEDIDO_PRODUCTO
            var c_id_ped=$(this).parent().parent().children().html();
            var c_valor_ped=$(this).parent().parent().children().next().next().html();
            var x=confirm("Desea eliminar el pedido seleccionado?");

            if(x==true){
                var pedido = { id_ped:c_id_ped,valor_ped:c_valor_ped };    
                $.ajax({
                    type: "POST",
                    url: "del_ped.php",
                    data: pedido,
                    success: function( data ) {
                        alert(data);
                        send_mail_delete(pedido);
                    },
                });
                //location.reload();
            }
            else{}
        });
        function send_mail_delete(dataPedido){
            dataPedido.paso = 'delete';
            $.ajax({
                type: "POST",
                url: "send_email_pedido.php",
                data: dataPedido
            });
        }
    </script>

    <?php 

    $fecha=date("y-m-d"); ?>

    <!-- ############################################################################################# -->
    <script language="javascript">
        $(document).ready(function() {
            $(".botonExcel").click(function(event) {
                $('td.acciones').remove();
                $("#datos_a_enviar").val( $("<div>").append( $("#lista_pago_func").eq(0).clone()).html());
                $("#FormularioExportacion").submit();
            });
        });
    </script>

    <div class="row" id="form_print" style="display: none; text-align:center;">
        <form action="../lib/ficheroExcel.php" method="post" id="FormularioExportacion" class="form-inline">
            <div class="modal-header">
                <h3 align="center">Exportar Pendientes</h3>
            </div>
            <div align="center">
                <input type="hidden" name="nombre" class="form-control" autocomplete="off" placeholder="Nombre del Fichero" value="CDMAG-Deben<?php echo $fecha; ?>" required><br>
                <input type="hidden" name="imp" class="form-control" value="excel">
                <!--
                <strong>Imprimir en: </strong><br>
                <select name="imp" class="form-control">
                    <option value="excel">Hoja de Excel</option>
                </select>-->
                <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                <button type="submit" class="btn btn-success botonExcel"> <strong>Imprimir Consolidado</strong></button>
                
            </div>
        </form>
        <!--<div class="col-sm-offset-3 col-xs-2 col-md-2 col-lg-2 col-sm-3 "><a href="ing_pag.php"><button style="width:5em; height:3em;" type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>-->
    </div>
<!-- ############################################################################################# -->

<!-- ############################################################################################# -->

    <script language="javascript">
        $(document).ready(function() {
            $(".botonExcel2").click(function(event) {
                $('td.acciones').remove();
                $("#datos_a_enviar2").val( $("<div>").append( $("#lista_pago_func_fecha").eq(0).clone()).html());
                $("#FormularioExportacion2").submit();
            });
        });
    </script>

    <div class="row" id="form_print2" style="display: none; text-align:center;">
        <form action="../lib/ficheroExcel.php" method="post" id="FormularioExportacion2" class="form-inline">
            <!--<div class="modal-header">
                <h3 align="center">Exportar pendientes por rango</h3>
            </div>-->
            <div align="center">
                <input type="hidden" name="nombre" class="form-control" autocomplete="off" placeholder="Nombre del Fichero" value="CDMAG-Deben<?php echo $fecha; ?>" required><br>
                <input type="hidden" name="imp" class="form-control" value="excel">
                <!--
                <strong>Imprimir en: </strong><br>
                <select name="imp" class="form-control">
                    <option value="excel">Hoja de Excel</option>
                </select>-->
                <input type="hidden" id="datos_a_enviar2" name="datos_a_enviar2" />
                <button type="submit" class="btn btn-success botonExcel2"><strong>Imprimir rango</strong></button>
                
            </div>
        </form>
        <div class="col-sm-offset-3 col-xs-2 col-md-2 col-lg-2 col-sm-3 "><a href="ing_pag.php" class="" ><button style="width:5em; height:3em;" type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>
    </div>
            
<!-- ############################################################################################# -->


    <div class="row"><!-- t_detalles_prod-->
        <div class="col-xs-1 col-md-2"></div>
        <div class="col-xs-10 col-md-8" style="text-align:center;">
            <div class="panel panel-default" id="t_detalles_prod" style="display: none; text-align:center;">
                <div class="panel-heading"><h4>Detalles de Pedido <span class="id_detal"></span></h4></div>
                <!-- Table -->
                <table class="table table-striped" style="width: 100%; text-align: center">
                    <!-- <table class="table table-striped">-->
                    <tr>
                        <td><b>Descripcion</b></td>
                        <td><b>Precio</b></td>
                        <td><b>Cantidad</b></td>
                    </tr>
                    <tbody id="body_t_producto"></tbody>
                </table>
            </div>
            <div class="col-xs-2 col-md-2 col-lg-2 col-sm-3 volver_ing_pag_2"><button type="button" class="btn btn-default btn-lg btn-block  detalle-atras">Atras.</button></div>
        </div>
        <div class="col-xs-1"></div>
    </div>

    <div class="row" id="pagos" style="display: none;">
        <div class="col-xs-3"></div>
        <div class="col-xs-6">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><h4>Pagos</h4></div>

                <form class="form-horizontal" style="margin:2em;">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Identificación</label>
                        <div class="col-sm-10 pag_cedula">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Nombre Completo</label>
                        <div class="col-sm-10 pag_nombres">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Valor a pagar</label>
                        <div class="col-sm-4">
                            <input type="number" class="form-control pag_valor" autofocus="" id="valor_pag" onkeyup="get_valor_pagar(this.value)" placeholder="Digite el valor en pesos $">
                        </div>
                        <label for="inputPassword3" class="col-sm-2 control-label">Observación</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control pag_observacion" id="pag_observacion" placeholder="Observación del Pago">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Total Deuda</label>
                        <div class="col-sm-10 pag_debe">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default registrar_pago" disabled>Registrar Pago</button>
                        </div>
                    </div>
                </form>              
            </div>
            <button type="button" class="btn btn-default btn-lg btn-block pago-atras">Atras</button>
        </div>
        <div class="col-xs-3"></div>
    </div>

    


    <div class="row" id="lista_pago_func_busq" style="display: block; text-align: center">
        <div class="col-xs-1"> </div>
        <div class="col-xs-5 col-md-5 col-lg-5 col-sm-10" style="text-align: center">
            <div class="panel panel-default" style="text-align: center;">
                <!-- Default panel contents -->
                <div class="panel-heading"><h4>Funcionarios con Cuentas Pendientes</h4></div>

                <table class="table table-striped" style="text-align: center">
                    <tr>
                        <td>
                            <label>Funcionario</label> <input type="text" class="form-control" autofocus="" id="cedula" name="cedula" size="15" onkeyup="get_funcionario(this.value)" placeholder="Cedula ó nombre del Funcionario">
                            <div id="nombre"></div>
                            <input type="hidden" id="usuarioSeleccionado" name="">
                        </td>
                        <td>
                            <label>Fecha (inicio)</label>  <input class="form-control f_inicio hide" type="date" name="fecha_i" min="2015-05-01" value='' >
                            <label>Fecha (Fin)</label> <input class="form-control f_fin" type="date" name="fecha_f" size="10" min='2015-05-01' value='' onchange="pedidos_usuario_fecha()"> <!-- value=<?php //echo $f_fin; ?> -->
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-xs-2 col-md-2 col-lg-2 col-sm-3 volver_ing_pag"><a href="ing_pag.php"><button type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>
        </div>
         <div class="col-xs-12 col-sm-3 " style="margin-top: 2em">
            <form action="rake.php" method="post" id="uploadCargaMasiva" enctype="multipart/form-data">
                <label for="cargaArchivo" class="text-center">
                    <i class="fas fa-file-upload" id="iconSubir" style="margin-left: 3em; font-size: 4em;"></i>
                    <strong style="margin-left: 1em; margin-top:-6em; position: absolute;" id="messageUpload">Selecciona el archivo</strong>
                </label>
                <input type="file" class="hide" accept=".csv" name="upload" id="cargaArchivo">
                <a href="../images/Formato_carga_masiva.csv" download="Formato_carga_masiva.csv"  class="btn btn-primary" style="position: absolute; bottom: -30px ; z-index: 3; margin-left: -9em"><i class="fas fa-file-download "></i><span class="left1">Descargar Formato valido</span></a>
            </form>
        </div>
        <div class="col-xs-1"></div>
        <!--<div class="col-xs-5" style="text-align:left">
            <h2>Informes</h2>
            <a href="">Detalles de Pendientes</a><br>
            <a href="">Detalles de Pagos</a>
        </div>-->
    </div>


    <div class="row" id="lista_por_funcionario" style="display:none; margin-top:5em;" >
        <div class="col-xs-1"></div>
        <div class="col-xs-5">
            <div class="panel panel-default" style="text-align: center;">
                <!-- Default panel contents -->
                <div class="panel-heading titulo-pedido-func" ><h4>Pedidos por Funcionario</h4></div>
               
                <table class="table table-striped" style="text-align: center">
                    <tr>
                        <td><b>ID pedido</b></td>
                        <td><b>Fecha</b></td>
                        <td><b>Valor</b></td>
                        <td class="acciones"><b>Acciones</b></td>
                    </tr>
                    <tbody id="body_t_pedido_usuario" style="text-align: center"></tbody>
                </table>
            </div>
        </div>
        <div class="col-xs-5">
            <div class="panel panel-default" style="text-align: center;">
                <!-- Default panel contents -->
                <div class="panel-heading"><h4>Registro de Pagos</h4></div>

                <table class="table table-striped" style="text-align: center">
                    <tr>
                        <td><b>#</b></td>
                        <td><b>Fecha</b></td>
                        <td><b>Valor</b>  <!--<img class="print_pagos_fun" src="../images/icon_svg/print44.svg" width="25em" height="25em" title="Exportar">--></td>
                        <td><b>Observación</b>
                    </tr>
                    <tbody id="body_t_reg_pagos" style="text-align: left"></tbody>
                </table>
            </div>
        </div>
        <div class="col-xs-1"></div>
    </div>
    <div class="row">
        <div class="col-sm-offset-1 col-xs-1 volver_ing_pag"><a href="ing_pag.php"><button type="button" class="btn btn-default btn-lg btn-block">Volver</button></a></div>
    </div>


    <div class="row" id="lista" style="display: block; text-align: center;">
        <div class="col-xs-1"> </div>
        <div class="col-xs-10" style="text-align: center">
            
            <div class="panel panel-default" style="text-align: center" id="lista_pago_func">
                <div class="panel-heading"><h4>Listado de Cuentas Pendientes</h4></div>
                <!-- Table -->
                <table class="table table-striped" style="width: 100%; text-align: center">
                    <!-- <table class="table table-striped">-->
                    <tr>
                        <td><b>Identificacion</b></td>
                        <td><b>Nombre Completo</b></td>
                        <td><b>Fecha</b></td>
                        <td><b>Pendiente</b></td>
                        <td class="acciones"><b>Acciones</b> <img class="print_deben_fun" src="../images/icon_svg/print44.svg" width="25em" height="25em" title="Exportar">
                    </tr>
                    <tbody id="tbody_listado_pendiente" style="text-align: left">
                        <?php
                        
                        /*$query=mysqli_query($conn,"SELECT f.id_funcionario, f.nombres,f.apellidos,t_saldo.t_saldo_fecha,t_saldo.t_saldo_valor
                                            FROM t_saldo
                                                JOIN funcionarios f ON(t_saldo.funcionarios_id_funcionario=f.id_funcionario)
                                            WHERE t_saldo.idt_saldo IN(SELECT max(ts.idt_saldo) as idt
                                                                        FROM t_saldo ts 
                                                                        GROUP BY ts.funcionarios_id_funcionario)
                                            AND t_saldo.t_saldo_valor>0
                                            ORDER BY f.nombres ASC");*/

                        $query=mysqli_query($conn,"SELECT  f.id_funcionario, f.nombres, f.apellidos, T.t_saldo_fecha, T.t_saldo_valor
                                FROM    funcionarios f
                                INNER JOIN t_saldo T ON T.funcionarios_id_funcionario = f.id_funcionario
                                WHERE   T.idt_saldo = ( 
                                                        SELECT  MAX( ts.idt_saldo ) AS idt
                                                        FROM    t_saldo ts
                                                        WHERE   ts.t_saldo_fecha
                                                        AND ts.funcionarios_id_funcionario = f.id_funcionario
                                                    )
                                AND T.t_saldo_valor >0 AND f.estado = true
                                ORDER BY `f`.`nombres` ASC");

                        while($row=mysqli_fetch_array($query,MYSQLI_BOTH)){
                            if ($row[4]==0 || $row[4]=="0") {
                               
                            }else{
                                echo "<tr id=\"pendiente-".$row[0]."\"><td class=\"c_id_func\">".$row[0]."</td><td>".$row[1]." ".$row[2]."</td ><td>".$row[3]."</td><td class=\"subtotal_ped_fun pendiente-".$row[0]." \">".$row[4]."</td><td class='acciones'><img class=\"pagar\" src=\"../images/icon_svg/money175.svg\" width=\"25em\" height=\"25em\" title=\"Realizar Pago\">    <img class=\"detalle_ped left1\" src=\"../images/icon_svg/write13.svg\" width=\"25em\" height=\"25em\" title=\"Ver Detalles\"></td></tr>";
                            }
                        }
                        ?>
                        <tr><td colspan="2"></td><td><b>Total:</b></td><td id="total_ped_fun"> </td><td></td></tr>
                    </tbody>
                </table>
            </div>


            <div class="panel panel-default" style="text-align: center; display:none;" id="lista_pago_func_fecha">
                <div class="panel-heading"><h4>Listado de Cuentas Pendientes fecha</h4></div>
                <!-- Table -->
                <table class="table table-striped" style="width: 100%; text-align: center">
                    <!-- <table class="table table-striped">-->
                    <tr>
                        <td><b>Identificacion</b></td>
                        <td><b>Nombre Completo</b></td>
                        <td><b>Fecha</b></td>
                        <td><b>Pendiente</b></td>
                        <td class="acciones"><b>Acciones</b> <img class="print_deben_fun" src="../images/icon_svg/print44.svg" width="25em" height="25em" title="Exportar">
                    </tr>
                    <tbody id="t_body_lpendientes_original" style="text-align: left">
                        <!-- CONTENIDO DINAMICO POR JQUER, RESULTADO DE FUNCIONARIOS QUE DEBEN-->
                    </tbody>
                    <tr><td colspan="2"></td><td><b>Total:</b></td><td id="total_ped_fun_busq"> </td><td></td></tr>
                </table>
            </div>


            <div class="col-xs-1 col-md-2 col-lg-1 col-sm-3 volver_ped"><a href="pedidos.php"><button type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>
        </div>
        <div class="col-xs-1"></div>
    </div>


    <?php   include 'footer.php';
}
else{
    header("Location:pedidos.php");
}
?>
<div >
    <div class="modal fade" tabindex="-1" role="dialog" id="loadFile">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <p>One fine body&hellip;</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>

