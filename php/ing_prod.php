<?php session_start(); 

$q = (isset($_POST['q']) ? "'%".$_POST['q']."%'" : "'%%'" ) ;
$pagina = (isset($_GET['pag']) ? $_GET['pag'] : 1 ) ;
$estado = (isset($_POST['estado']) ? $_POST['estado'] : 1 ) ;
$estado = ($estado == 2) ? "'%%'" : $estado ;
$limit = 20;
$offset =($limit * $pagina) - $limit;
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php"); 
}elseif ( isset($_SESSION['id_sesion']) ) {
	include 'header.php';
    include 'conn.php';
    date_default_timezone_set('America/Bogota');
?>
    <script> 
        $(document).ready(function(){
            $(".nuevo_prod").live('click',function(){
                $("#lista_prod").css("display", "none");
                $("#upd_prod").css("display", "none");
                $("#filtroBusqueda").css("display", "none");
                $("ul.pagination").css("display", "none");
                $("#registro_prod").css("display", "block");
            });

            $(".registrar_prod").live('click',function(){
                var c_prec=$(".precio").val();
                var c_desc=$(".descrip").val();

                //alert("c prec "+c_prec+" c desc "+c_desc);

                $.post("ins_prod.php",{prec:c_prec, descrip:c_desc, opc:'ins'},function(response){
                    if (response) {
                        alert(response);    
                    };

                    $("#lista_prod").css("display", "block");
                    $("#upd_prod").css("display", "none");
                    $("#registro_prod").css("display", "none");
                });
                location.reload();
            });

            $(".eliminar_prod").live('click',function(){
                var confi=confirm("¿Desea eliminar este producto?");
                if (confi==true) {
                    var c_id_prod=$(this).attr('identificador');

                    $.post("ins_prod.php",{id_prod:c_id_prod, opc:'del'},function(data){
                        alert(data);
                    });
                    location.reload();
                }
                else{}
            });
             $(".activar_pro").live('click',function(){
                var confi=confirm("¿Desea activar este producto?");
                if (confi==true) {
                    var c_id_prod = $(this).attr('identificador');

                    $.post("ins_prod.php",{id_prod:c_id_prod, opc:'activar'},function(data){
                        alert(data);
                    });
                    location.reload();
                }
            });
            

            $(".editar_prod").live('click',function(){
                var id=$(this).parent().parent().children().next().html();
                var descripcion=$(this).parent().parent().children().next().next().html();
                var valor=$(this).parent().parent().children().next().next().next().html();

                $("#lista_prod").css("display", "none");
                $("#upd_prod").css("display", "block");
                $("#registro_prod").css("display", "none");
                $(".registrar_prod").css("display","none");
                $("#filtroBusqueda").css("display", "none");
                $("ul.pagination").css("display", "none");

                $(".upd_precio").val(valor);
                $(".upd_descrip").val(descripcion);
                $(".upd_id_produ").val(id);
            });

            $(".upd_prod").live('click',function(){
                var c_prec=$(".upd_precio").val();
                var c_id_produ=$(".upd_id_produ").val();

                //alert(c_prec);
                //alert(c_id_produ);

                if(c_prec){
                    $(".registrar_prod").css("display","none");

                    $.post("ins_prod.php",{precio:c_prec, id_prod:c_id_produ, opc:'upd'},function(response){
                        alert(response);
                        
                        $("#lista_prod").css("display", "block");
                        $("#upd_prod").css("display", "none");
                        $("#registro_prod").css("display", "none");
                    });
                    location.reload();
                }else{}
            });
        });
    </script>

    <script>
    function get_prec_nuevo_prod(und) {

        if(und==""){
            $(".registrar_prod").attr("disabled",true);
        }else if (und=="NaN"){
            $(".registrar_prod").attr("disabled",true);
        }else{
            $(".registrar_prod").attr("disabled",false);
        }
    }

    function get_prod_prec(und){
        var c_desc_prod=$(".descrip").val();

        if(und==""){
            $(".precio").attr("disabled",true);
        }else if (und=="NaN"){
            $(".precio").attr("disabled",true);
        }else{
            $.post("ins_prod.php",{descrip:c_desc_prod, opc:'val'},function(dato1){
                //alert(dato1);
                if (dato1) {
                    alert(dato1);
                   $(".precio").attr("disabled",true); 
                }else{
                    $(".precio").attr("disabled",false); 
                }
            });
        }
    }

    </script>

    <div class="row" id="filtroBusqueda">
        <form action="ing_prod.php" method="post">
            <div class="col-xs-12 col-sm-2 col-sm-offset-7">
                <div class="form-group">
                    <select class="form-control" name="estado">
                        <option value="" disabled="" selected="" >Estado</option>
                        <option <?= ($estado == 1) ? 'selected' : '' ?> value="1">Activo</option>
                        <option <?= ($estado == 0) ? 'selected' : '' ?> value="0">Inactivo</option>
                        <option <?= (isset($_POST['estado']) && $_POST['estado'] == 2) ? 'selected' : '' ?> value="2">Todos</option>
                    </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-2">
                <div class="input-group">
                    <input autocomplete="off" type="text" value="<?= isset($_POST['q']) ? $_POST['q'] : ''?>" name="q" class="form-control" placeholder="Filtrar por nombre del producto">
                    <span class="input-group-btn">
                        <button class="btn btn-success" type="submit">Buscar</button>
                    </span>
                </div>
            </div>
        </form>
    </div>
    
    <div class="row" id="registro_prod" style="display: none; text-align:center;">
        <div class="col-xs-1"> </div>
        <div class="col-xs-10" style="text-align:center;">
            <div class="panel panel-default">
                    <!-- Default panel contents -->
                <div class="panel-heading"><h4>Registrar Nuevo Producto</h4></div>

                <form class="form-horizontal" style="margin:2em;">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Descripción del Producto</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="text" class="form-control descrip" autofocus="" id="descrip" placeholder="Descripción o nombre del producto" onkeyup="get_prod_prec(this.value)">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Precio</label>
                        <div class="col-sm-10 col-md-8">
                            <input type="number" class="form-control precio" id="precio" placeholder="Precio de venta al público" onkeyup="get_prec_nuevo_prod(this.value)" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xm-8 col-sm-10">
                            <button type="submit" class="btn btn-default registrar_prod" disabled>Registrar</button>
                        </div>
                    </div>
                </form>
                <div class="col-xs-2 col-md-2 col-lg-2 col-sm-3 volver_ing_prod"><a href="ing_prod.php"><button type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>
            </div>
        </div>
        <div class="col-xs-1"> </div>
    </div>

    <div class="row" id="upd_prod" style="display: none; text-align:center;">
        <div class="col-xs-2"> </div>
        <div class="col-xs-8" style="text-align:center;">
            <div class="panel panel-default">
                    <!-- Default panel contents -->
                <div class="panel-heading"><h4>Modificar Producto</h4></div>

                <form class="form-horizontal" style="margin:2em;">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Descripción</label>
                        <div class="col-sm-10">
                            <input type="text" disabled class="form-control upd_descrip" id="descrip" placeholder="descripcion">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Precio</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control upd_precio" id="upd_precio" placeholder="precio">
                            <input type="hidden" class="form-control upd_id_produ" id="upd_id_produ" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default upd_prod">Aplicar</button>
                        </div>
                    </div>
                </form>
                <div class="col-xs-2 col-md-2 col-lg-2 col-sm-3 volver_ing_prod"><a href="ing_prod.php"><button type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>
            </div>
        </div>
        <div class="col-xs-2"> </div>
    </div>

	<div class="row" id="lista_prod" style="display: block; text-align:center;">
		<div class="col-xs-1"> </div>
            <div class="col-xs-10" style="text-align:center;">
                <div class="panel panel-default">
                    <!-- Default panel contents -->
                    <div class="panel-heading"><h4>Listado de Productos</h4></div>

                    <!-- Table -->
                    <table class="table" style="text-align:center;">
                       <!-- <table class="table table-striped">-->
                        <tr>
                            <td class="col-xs-1"><b>Estado</b></td>
                            <td><b>Item</b></td>
                            <td><b>Descripcion del Producto</b></td>
                            <td><b>Precio</b></td>
                            <td><b>Acciones</b> <img class="nuevo_prod" src="../images/icon_svg/add182.svg" width="20em" height="20em" title="Nuevo Producto"></td>
                        </tr>
                        <tbody id="body_t_productos" style="text-align:left;">
                        <?php
                        $sql_count = "SELECT COUNT(id_producto) AS cantidad FROM productos WHERE estado LIKE ".$estado." AND descripcion LIKE $q";
                        $count=mysqli_query($conn,$sql_count);
                        $count = $count->fetch_array(MYSQLI_NUM);
                        $cantidadFuncionarios = $count[0];
                        $numPaginas = ceil($count[0] / $limit);

                        $query=mysqli_query($conn,"SELECT * FROM productos WHERE descripcion LIKE $q AND estado LIKE ".$estado." ORDER BY descripcion ASC  LIMIT $limit offset $offset");
                        while($row=mysqli_fetch_array($query,MYSQLI_BOTH)){ ?>
                           <tr>
                                <td class="text-center">
                                    <?= ($row[3] == 0) ? "<i class='fas cursor size2 fa-certificate red eliminar_fun' title='Producto inactivo'></i>" : " <i class='fas cursor size2 fa-certificate green activar_fun' title='Producto activo'></i>" ; ?>
                                </td>
                                <td style="text-align:center;"><?= $row[0] ?></td>
                                <td><?= $row[1] ?></td><td><?= $row[2] ?></td>
                                <td style="text-align:center;">
                                    <?= ($row[3] == 1) ? "<i class='fas cursor size2 fa-trash-alt red eliminar_prod' title='Eliminar producto' identificador=".$row[0]."></i>" : " <i class='fas cursor size2 fa-check-square green activar_pro' title='Activar producto' identificador=".$row[0]."></i>" ; ?>
                                    <i class="fas fa-edit editar_prod cursor size2 left1"></i>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-2 col-md-2 col-lg-1 col-sm-3"><a href="pedidos.php"><button type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div> 
            </div>
		</div>
		<div class="col-xs-1"></div>
	</div>
<?php   include 'footer.php';
}
else{
  header("Location:pedidos.php"); 
}
?>
<ul class="pagination">
    <?php for ($i = 1 ; $i <= $numPaginas ; $i++) {  ?>
        <li><a href="ing_prod.php?pag=<?= $i ?>" class="paginacion <?= ($_GET['pag'] == $i) ? 'bg-green' : '' ?>" pag="<?= $i ?>"><?= $i ?></a></li>
    <?php } ?>
</ul>

