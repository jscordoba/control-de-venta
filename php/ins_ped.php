<?php
session_start();

if (!isset($_SESSION["id_sesion"])){
    header("Location:index.php");
}else{
    include 'conn.php';
    include 'enviar_correo.php';
    date_default_timezone_set('America/Bogota');
    $id_productos=$_REQUEST["ides"];
    $cantidades=$_REQUEST["cantidades"];
    $valor_pedido=$_REQUEST["valor"];
    $id_user = $_REQUEST["id_user"];
    $valor_compras = $_REQUEST["valor_compras"];

    //SE OBTIENE EL EMAIL DEL FUNCIONARIO
    $q_email=mysqli_query($conn,"SELECT fun_email FROM funcionarios WHERE id_funcionario='".$id_user."'"); 
    $row_email=mysqli_fetch_array($q_email,MYSQLI_BOTH);

    //SE OBTIENE EL SALDO DEL FUNCIONARIO A ACTUALIZAR
    //$query=mysqli_query($conn,"SELECT * FROM t_saldo WHERE funcionarios_id_funcionario='".$id_user."' GROUP BY t_saldo_fecha ORDER BY t_saldo_fecha DESC");//ver si funcionarios deben
    $query=mysqli_query($conn,"SELECT t_saldo.* FROM funcionarios f 
                                    INNER JOIN (
                                        SELECT max(ts.idt_saldo) as idt,ts.funcionarios_id_funcionario
                                        FROM t_saldo ts 
                                        WHERE ts.funcionarios_id_funcionario='$id_user'
                                    ) as saldo ON(f.id_funcionario=saldo.funcionarios_id_funcionario)
                                    INNER JOIN ( t_saldo ) ON (t_saldo.idt_saldo=saldo.idt)");

    $row_query=mysqli_fetch_array($query,MYSQLI_BOTH);

    //#################SI NO HAY UN SALDO O ESTA EN 0 SE INSERTA EL NUEVO SALDO#######################
    if ($row_query[1]=='0' OR $row_query[1]==0 OR $row_query[1]=='' OR $row_query[1]==null) {
    
        $sql2="INSERT INTO t_saldo (idt_saldo,t_saldo_valor,t_saldo_fecha, funcionarios_id_funcionario) VALUES(null, '".$valor_pedido."', '".date('Y-m-d')."', '".$id_user."')";
        $sql3=mysqli_query($conn,$sql2); //INSERTAR MOVIMIENTO DE SALDO EN TABLA SALDOS

        //VALIDATION SI SE INSERTA SALDO CORRECTAMENTE
        if ($sql3) {
            //SE INSERTA EN LA TABLA PEDIDOS_PLANTA PARA SECCIONAR Y DIVIDIR LAS VENTAS DE LAS PLANTAS (22-08-16)
            if ($_SESSION["id_sesion"]=="ventasp2") {
                $planta="PL2";
            }elseif ($_SESSION["id_sesion"]=="venta") {
                $planta="PL1";
            }elseif ($_SESSION["id_sesion"]=="ventaszf") {
                $planta="PLZF";
            }elseif ($_SESSION["id_sesion"]=="admin") {
                $planta="ADM";
            }

            //SE INSERTA EN LA TABLA PEDIDOS EL VALOR Y FECHA DEL PEDIDO TOTAL DEL FUNCIONARIO
            $sq=mysqli_query($conn,"INSERT INTO pedidos(fecha, valor, id_funcionario,pedido_planta)VALUES('".date('Y-m-d')."', '".$valor_pedido."', '".$id_user."','".$planta."')");
            
            if ($sq) {//SE VALIDA SI SE INSERTA PEDIDO OK
                $id=mysqli_insert_id($conn);//sacar id para el pedido

                //SE SEPARAN LOS PRODUCTOS DEL PEDIDO Y LAS CANTIDADES
                $ides_productos=explode(",",$id_productos);
                $x_cantidades=explode(",",$cantidades);

                $cant=count($ides_productos);

                //SE RECORRE LA LISTA DE PRODUCTOS DEL PEDIDO Y SE INSERTA UNO A UNO EN LA TABLA PEDIDO_PRODUCTO
                for($i=0;$i<$cant;$i++){
                    try {
                        $sql0="INSERT INTO pedido_producto(id_pedido, id_producto, cantidad, precio_unidad)VALUES('".$id."' ,'".$ides_productos[$i]."', '".$x_cantidades[$i]."', '".$valor_compras[$i]."')"; //guardar registro de productos por pedido de usuario
                        $sql1=mysqli_query($conn,$sql0);

                    } catch (Exception $e) {
                        echo "Error Al insertar en tabla de productos. Favor informar al área de TICs\n".mysqli_errno($conn) . ": " . mysqli_error($conn) . "\n";
                    }
                }
                echo $id.",";//RESPUESTA A FUNCTION ingresar_pedido()>styles.php
            }else{
                echo "Error Al insertar en tabla de pedidos. Favor informar al área de TICs\n".mysqli_errno($conn) . ": " . mysqli_error($conn) . "\n";
            }

            //enviar_correo($id_user,$valor_pedido,$row_email[0],$id); //enviar notificacion a correo
            echo $id;//RESPUESTA A FUNCTION ingresar_pedido()>styles.php
        }else{
            echo "Error Al insertar en tabla de saldos. Favor informar al área de TICs\n".mysqli_errno($conn) . ": " . mysqli_error($conn) . "\n";
        }
    }else{//############SI HAY UN SALDO EXISTENTE SE TOMA EL SALDO BASE Y SE INSERTA EL NUEVO SALDO#################
        $valor_deb=intval($row_query[1]);//GET VALUE ACTUAL OF DB
        $valor_add=intval($valor_pedido);//GET VALUE ACTUAL PEDIDO
        $add_pedido=$valor_deb+$valor_add;//PLUS ACTUAL DB + WHIT PEDIDO

        //INSERTAR EL SALDO ACTUALIZADO EN LA TABLA SALDOS
        $sql4="INSERT INTO t_saldo (idt_saldo,t_saldo_valor,t_saldo_fecha, funcionarios_id_funcionario) VALUES(null, '".$add_pedido."', '".date('Y-m-d')."', '".$id_user."')";
        $sql5=mysqli_query($conn,$sql4);

        //VALIDATION SI SE INSERTA SALDO OK
        if ($sql5) {

            //SE INSERTA EN LA TABLA PEDIDOS_PLANTA PARA SECCIONAR Y DIVIDIR LAS VENTAS DE LAS PLANTAS (22-08-16)
            if ($_SESSION["id_sesion"]=="ventasp2") {
                $planta="PL2";
            }elseif ($_SESSION["id_sesion"]=="venta") {
                $planta="PL1";
            }elseif ($_SESSION["id_sesion"]=="ventaszf") {
                $planta="PLZF";
            }elseif ($_SESSION["id_sesion"]=="admin") {
                $planta="ADM";
            }

            //SE INSERTA EN LA TABLA PEDIDOS EL VALOR Y FECHA DEL PEDIDO TOTAL DEL FUNCIONARIO
            $sq=mysqli_query($conn,"INSERT INTO pedidos(fecha, valor, id_funcionario,pedido_planta)VALUES('".date('Y-m-d')."', '".$valor_pedido."', '".$id_user."','".$planta."')");
                        
            if ($sq) {//SE VALIDA SI SE INSERTA PEDIDO OK
                $id=mysqli_insert_id($conn);//sacar id para el pedido

                //SE SEPARAN LOS PRODUCTOS DEL PEDIDO Y LAS CANTIDADES
                $ides_productos=explode(",",$id_productos);
                $x_cantidades=explode(",",$cantidades);

                $cant=count($ides_productos);

                //SE RECORRE LA LISTA DE PRODUCTOS DEL PEDIDO Y SE INSERTA UNO A UNO EN LA TABLA PEDIDO_PRODUCTO
                for($i=0;$i<$cant;$i++){
                    try {
                        $sql0="INSERT INTO pedido_producto(id_pedido, id_producto, cantidad, precio_unidad)VALUES('".$id."' ,'".$ides_productos[$i]."', '".$x_cantidades[$i]."', '".$valor_compras[$i]."')"; //guardar registro de productos por pedido de usuario
                        $sql1=mysqli_query($conn,$sql0);

                    } catch (Exception $e) {
                        echo "Error Al insertar en tabla de productos. Favor informar al área de TICs\n".mysqli_errno($conn) . ": " . mysqli_error($conn) . "\n";
                    }
                }
                echo $id.",";//RESPUESTA A FUNCTION ingresar_pedido()>styles.php
            }else{
                echo "Error Al insertar en tabla de pedidos. Favor informar al área de TICs\n".mysqli_errno($conn) . ": " . mysqli_error($conn) . "\n";
            }

            //enviar_correo($id_user,$valor_pedido,$row_email[0],$id); //enviar notificacion a correo
            echo $id;//RESPUESTA A FUNCTION ingresar_pedido()>styles.php
        }else{
            echo "Error Al insertar en tabla de saldos. Favor informar al área de TICs\n".mysqli_errno($conn) . ": " . mysqli_error($conn) . "\n";
        }            
    }

    
	//enviar_correo($id_user,$valor_pedido,$row_email[0],$id); //enviar notificacion a correo
    //http://localhost/cdmag/php/ins_ped.php?id_user=10880044252 fundacion 
}
?>
