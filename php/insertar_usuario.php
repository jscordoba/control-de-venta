<?php 
	session_start();
	if (!isset($_SESSION["id_sesion"])){
	    header("Location:index.php");
	}elseif ( isset($_SESSION['id_sesion']) ) {
	    include 'header.php';
	    include 'conn.php';
	    date_default_timezone_set('America/Bogota');
	   	if (isset($_GET['id'])) {
		    $sql_user = "SELECT * FROM funcionarios WHERE id_funcionario = ".addslashes(($_GET['id']));
		    $sql_user = mysqli_query($conn,$sql_user); 
		    $user = mysqli_fetch_array($sql_user);
		}

?>
<div class="row">
	<form action="<?= (isset($user['id_funcionario'])) ? "update_fun.php" : "ins_fun.php" ?>" id="inserta_usuario">
		<div class="col-xs-8 col-xs-offset-2">
			<div class="panel panel-primary">
  				<div class="panel-heading">Insertar nuevo usuario <button type="submit" id="" class="btn-success btn absolute-right">Guardar funcionario</button></div>
  					<div class="panel-body">
						<div class="row top1">
							<div class="col-xs-6">
								<label for="sel1"> Nombres * </label>
								<div class="input-group">
								  	<span class="input-group-addon" id="basic-addon1"><i class="fas fa-user"></i></span>
								  	<input required="" value="<?= (isset($user['nombres'])) ? $user['nombres'] : "" ?>" id="" type="text" autocomplete="off" name="nombres" class="form-control heigth2" placeholder="Nombres" aria-describedby="basic-addon1">
								</div>
							</div>
							<div class="col-xs-6">
								<label for="sel1"> Apellidos * </label>
								<div class="input-group">
								  	<span class="input-group-addon" id="basic-addon1"><i class="fas fa-user"></i></span>
								  	<input required="" value="<?= (isset($user['apellidos'])) ? $user['apellidos'] : "" ?>" id="" type="text" autocomplete="off" name="apellidos" class="form-control heigth2" placeholder="Apellidos" aria-describedby="basic-addon1">
								</div>
							</div>
						</div>
						<div class="row top1">
							<div class="col-xs-6">
								<label for="sel1"> Email * </label>
								<div class="input-group">
								  	<span class="input-group-addon" id="basic-addon1"><i class="fas fa-envelope"></i></span>
								  	<input  value="<?= (isset($user['fun_email'])) ? $user['fun_email'] : "" ?>" id="" type="email" autocomplete="off" name="email" class="form-control heigth2" placeholder="Email funcionario" aria-describedby="basic-addon1">
								</div>
							</div>
							<div class="col-xs-6">
								<label for="sel1"> Cedula * </label>
								<div class="input-group">
								  	<span class="input-group-addon" id="basic-addon1"><i class="fas fa-address-card"></i></span>
								  	<input required="" <?= (isset($user['id_funcionario'])) ? "readonly" : "" ?> value="<?= (isset($user['id_funcionario'])) ? $user['id_funcionario'] : "" ?>" id="" type="text" autocomplete="off" name="id_fun" class="form-control heigth2" placeholder="Cedula del funcionario" aria-describedby="basic-addon1">
								</div>
							</div>
						</div>
						<div class="row top1">
							<div class="col-xs-6">
								<div class="form-group">
									<label for="sel1"> Listado de areas * </label>
									<select class="form-control" name="area" id="">
										<?php 
										$result=mysqli_query($conn, "SELECT * FROM funcionarios WHERE estado = true AND area != '' GROUP BY area ORDER BY area ASC");
				          				$num_areas=mysqli_num_rows($result); ?>
				          				<option class="font2" disabled="" selected="">Selecciona el area </option>
				 						<?php while ($areas=mysqli_fetch_array($result)) { ?>
											<option class="font2" <?= (isset($user['area']) && $areas['area'] == $user['area']) ? "selected" : ""?> value="<?= $areas['area'] ?>"><?= $areas['area'] ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
				  	</div>
				</div>
			</div>
		</div>
	</form>
</div>
<div class="row">
	<div class="col-xs-12">
		<div id="precarga" class="hide">
			
		</div>
	</div>
	<div class="col-xs-12 text-center">
		<div id="precarga" class="" style="margin-top: 2em">
			<span class="red hide" id="message" style="background: white; font-size: 2em; padding: .5em 1em; "></span>
		</div>
	</div>
</div>
<?php }else{
	header("Location:index.php");
} ?>
