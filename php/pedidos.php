<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}else{ 
?>
<!DOCTYPE HTML>
	<html>
	<head>
		<title>FodeMag - Opciones</title>
		<?php
			include 'header.php';
		?>
	</head>
	<body>
		<div class="total-content" style="vertical-align:middle; margin: 0; text-align: center;">
			<div class="row" style="margin: 0; text-align: center;">
                <div class="col-xs-1"></div>
				<div class="col-xs-10" style="margin: 0; text-align: center;">

                 <div class="container" style="margin-top:5%;">
						<div class="row">
							<div class="col-xs-3"></div>
							<div class="col-xs-6" style="vertical-align:middle;">
								<?php if ($_SESSION['id_sesion']=='venta') { ?>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="query.php"><button type="button" class="btn btn-primary btn-lg btn-block">Ingresar Pedidos</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="ventas_dia.php?pl=PL1"><button type="button" class="btn btn-default btn-lg btn-block">Ventas del Día</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="logout.php"><button type="button" class="btn btn-default btn-lg btn-block">Cerrar Sesión</button></a></div>
								<?php } ?>
								<?php if ($_SESSION['id_sesion']=='ventasp2') { ?>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="query.php"><button type="button" class="btn btn-primary btn-lg btn-block">Ingresar Pedidos</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="ventas_dia.php?pl=PL2"><button type="button" class="btn btn-default btn-lg btn-block">Ventas del Día</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="logout.php"><button type="button" class="btn btn-default btn-lg btn-block">Cerrar Sesión</button></a></div>
								<?php } ?>
								<?php if ($_SESSION['id_sesion']=='fodemag') { ?>
									<div class="col-xs-12" style="margin-top: 1em;"><a href="ing_pag.php"><button type="button" class="btn btn-primary btn-lg btn-block">Movimientos</button></a></div>
									<div class="col-xs-12" style="margin-top: 1em;"><a href="ing_prod.php"><button type="button" class="btn btn-default btn-lg btn-block">Productos</button></a></div>
									<div class="col-xs-12" style="margin-top: 1em;"><a href="ing_func.php"><button type="button" class="btn btn-default btn-lg btn-block">Funcionarios</button></a></div>
									<div class="col-xs-12" style="margin-top: 1em;"><a href="ventas_dia.php?pl=all"><button type="button" class="btn btn-default btn-lg btn-block">Ventas del Día</button></a></div>
									<div class="col-xs-12" style="margin-top: 1em;"><a href="Reportes.php"><button type="button" class="btn btn-default btn-lg btn-block">Reportes</button></a></div>
									<div class="col-xs-12" style="margin-top: 1em;"><a href="logout.php"><button type="button" class="btn btn-default btn-lg btn-block">Cerrar Sesión</button></a></div>
								<?php } ?>
								<?php if ($_SESSION['id_sesion']=='admin') { ?>
                                    <div class="col-xs-10" style="margin-top: 1em;"><a href="query.php"><button type="button" class="btn btn-primary btn-lg btn-block">Ingresar Pedidos</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="ing_pag.php"><button type="button" class="btn btn-default btn-lg btn-block">Movimientos</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="ing_prod.php"><button type="button" class="btn btn-default btn-lg btn-block">Productos</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="ing_func.php"><button type="button" class="btn btn-default btn-lg btn-block">Funcionarios</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="ventas_dia.php?pl=all"><button type="button" class="btn btn-default btn-lg btn-block">Ventas del Día</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="Reportes.php"><button type="button" class="btn btn-default btn-lg btn-block">Reportes</button></a></div>
									<div class="col-xs-10" style="margin-top: 1em;"><a href="logout.php"><button type="button" class="btn btn-default btn-lg btn-block">Cerrar Sesión</button></a></div>
								<?php } ?>
							</div>
							<div class="col-xs-3 col-sm-1 col-lg-3"></div>
						</div>
					</div><!-- -->

                    <div class="col-xs-1"></div>
				</div>
			</div>
		<?php include 'footer.php' ?>
		</div>
	</body>
	</html>
	<?php
	} 
?>