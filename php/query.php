<style>
  select.default{
    max-height: 7em;
    min-height: 7em;
  }
  option {
    font-size: 1.4em
  }
  input.heigthMax{
    font-size: 1.5em;
  }
  label{
    font-size: 1.4em;
  }
  .input-group-addon{
    background: #1B242F;
    color: white;
  }
  .input-group-addon span,.input-group-addon i{
    color: white;
  }
  #count_view_local_storage{
    position: absolute;
    right: 40px;
    top: 63px;
    z-index: 3;
  }
  .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus{
    background: rgba(232,44,12,.8) !important;
  }
</style>
<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php"); 
}elseif ($_SESSION['id_sesion']=='venta' OR $_SESSION['id_sesion']=='admin' OR $_SESSION['id_sesion']=='ventasp2') {

 
  include 'header.php';
  include 'conn.php';
?>

      <script>
          $(document).ready(function(){
            //remueve los pedidos almacenados en local cuidado
            //localStorage.removeItem('pedidos');
            $(".agregar_prod").attr("disabled", true);
            $(".cantidad_prod").attr("disabled", true);
            $(".enviar_ped").attr("disabled", true);

            if("pedidos" in localStorage){
              var datosLocal = localStorage.getItem("pedidos");
              var pedidos = datosLocal.split("/**/");
              $('#view_local_storage').removeClass('hide').text((pedidos.length - 1));
            } 
            
            $(".desc_prod").change(function() { //AL SELECCIONAR UN PRODUCTO SE ACTIVA LA CANTIDAD
              $(".cantidad_prod").attr("disabled",false);
              $(".cantidad_prod").attr("value","1");
            });

            $(".list_areas").change(function() {//AL SELECCIONAR UNA AREA SE ACTIVAN LOS FUNCIONARIOS
                $(".list_funcionarios").attr("disabled",false);
            });

            $('#search_producto').keyup(function(){
              if ($("#id_usuario").val() != '' ) {
                var data = $('#search_producto').val().toUpperCase();
        
                $.post("get_pro.php",{ producto: data },function(response){
                      var productoSearch = JSON.parse(response);
                      var html = '';
                      var cantidad = Object.keys(productoSearch).length
                      $('.desc_prod').html("");

                      $.each(productoSearch, function( index, value ) {
                          html = '<option id_producto="'+value['id_producto']+'" costo="'+value['precio']+'" value="'+value['descripcion']+'">'+value['descripcion']+'</option>';
                          $('.desc_prod').append(html).attr("disabled", false);
                      });
                      $("#cantidad").attr("disabled", false);
                      get_precio(productoSearch[0]['descripcion']);
                });
              }else{
                alert("Selecciona el usuario para realizar el pedido");
                $("#search_producto").val("");
              }
            });

            $('select.desc_prod').on('change', function(event) {
              event.preventDefault();
              var value = $(this).val();
              var costo = $('option:selected', this).attr('costo');
              var id_producto = $('option:selected', this).attr('id_producto');
              get_precio(value,costo,id_producto);
            });

            $('#search_user').keyup(function(){
            var data = $('#search_user').val().toUpperCase();              
                $('.desc_prod').removeAttr('disabled');
                    $("#cantidad").attr("disabled", false);
                    get_funcionarios($(".list_areas").val(), data);
                    $("#id_usuario").closest('div').removeClass("hide");
                if ( data == '') {
                  get_funcionarios($(".list_areas").val());
                  $("#search_producto").attr("disabled",false);
                }
            });

            $(".list_funcionarios").change(function() {//AL SELECCIONAR UN FUNCIONARIO SE ACTIVAN LOS PRODUCTOS
              var nombre=$(".list_funcionarios").html();
              if (nombre=="Selecione área" || nombre=="") {
                $(".desc_prod").attr("disabled",true);
              }else{
                $(".desc_prod").attr("disabled",false);
              }
            });

            $(".eliminar_pedido").live('click',function(){//AL SELECCIONAR UN PRODUCTO SE ACTIVA LA CANTIDAD
              var conf=confirm("¿Desea retirar el producto?");

              if(conf==true){
                $(this).parent().parent().remove();
                calcula_total();
              }
              else{}
            });

          });

          function btn_enviar(){
            var p_prod=$(".prod_ped").html();
            if (p_prod==null) {
              $(".enviar_ped").attr("disabled", true);
            };
          }
            
      </script>

<div class="cargando centrarContent hide">
  <i class="fas fa-spinner fa-spin "></i>
  <small class=" left1">Enviando mensaje</small>
</div>

<div class="row" id="count_view_local_storage">
  <ul class="nav nav-pills" role="tablist" >
    <li role="presentation" class="active"><a href="view_local_storage.php">Pedidos almacenados en local <span class="badge" id="view_local_storage"></span></a></li>
  </ul>
</div>

<div class="row top1" style="padding-left: 1em">
  <div class="col-xs-3 top1" >
    
    <div class="panel panel-primary top1">
      <div class="panel-heading text-left">LIstado de areas</div>
      <div class="panel-body">
        <div class="form-group" style="text-align:left; margin:0; margin-right:0.5em;">

          <?php
          $result=mysqli_query($conn, "SELECT * FROM funcionarios WHERE estado = true GROUP BY area ORDER BY area ASC");
          $num_areas=mysqli_num_rows($result); ?>

          <select class="form-control heigthMax list_areas" name="list_areas" onchange="get_funcionarios(this.value)">;
            <option selected="" value="" disabled="" >Selecciona el area</option>"
          <?php
          $i=0;
          while ($areas=mysqli_fetch_array($result)) {
          echo "<option value='".$areas['area']."'>".$areas['area']."</option>";
          $i++;
          }
          ?>
          </select>
        </div>
      </div>
    </div>
    <div class="panel panel panel-primary  view_filtro_user">
      <div class="panel-heading">Filtro de funcionarios</div>
        <div class="panel-body">
          <div class="input-group" style="text-align:left; margin:0; margin-right:0.5em;">
            <span class="input-group-addon" id="basic-addon1">
              <i class="fas fa-id-card"></i>
            </span>
            <input type="" placeholder="Filtro de funcionarios" value="" id="search_user" class="heigthMax form-control">
          </div>
      </div>
    </div>
    
    <div class="panel panel-primary">
      <div class="panel-heading text-left">Listado de personas del area</div>
      <div class="panel-body">
        <div class="form-group" style="text-align:left; margin:0; margin-right:0.5em;">
                <?php
            echo "<select class=\"form-control heigthMax list_funcionarios\" id=\"list_funcionarios\" name=\"list_funcionarios\" onchange=\"get_id_usuario(this.value)\" disabled>
                  <option default>Selecione el área</option>";
          ?>
          </select>
        </div>
      </div>
    </div>
    <div class="panel panel panel-primary">
      <div class="panel-heading">Usuario seleccionado</div>
      <div class="panel-body">
        <div class="input-group" style="text-align:left; margin:0; margin-right:0.5em">
          <span class="input-group-addon" id="basic-addon1">
            <i class="fas fa-user-tie"></i>
          </span>
          <input type="" readonly="" value="" class="heigthMax form-control" id="id_usuario">
        </div>
      </div>
    </div>
  </div>
  <div class="col-xs-9 top1">
    <div class="form-inline top1" name="t_agregar_pedido" style="text-align:center; margin:0;"><!-- CONTIENE TODO EL REGISTRO DEL PEDIDO -->
      <div class="row top1">
        <div class="col-xs-12">
          <div class="panel panel-primary">
            <div class="panel-heading">Información del pedido</div>
              <div class="panel-body">
                <div class="container" style="padding: 1em">
                  <div class="row">
                    <div class="col-xs-12" style="text-align:left;padding:0;margin-left:0;"><!-- SEGUNDA SECCION DE PEDIDOS -->
                      <div class="col-xs-12" style="padding:0;margin-left:0;padding-right:1em;padding-bottom:1em;">
                        <div class="row">
                            <div class="col-xs-12">
                              <div class="form-group" >
                                <label for="l_cantidad">Filtro de productos</label><br>
                                <input type="text" class="form-control " autocomplete="" id="search_producto"  placeholder="Filtro" >
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <label for="l_producto">Producto</label><br>
                            <select class="form-control desc_prod" name="producto" disabled>
                                <option selected disabled="">Selecciona el producto </option>
                                <?php
                                $result=mysqli_query($conn, "SELECT * FROM productos ORDER BY descripcion ASC  LIMIT 50 ");
                                while($row=mysqli_fetch_array($result,MYSQLI_BOTH)){
                                    ?><option id_producto="<?php echo($row['id_producto']); ?>" costo="<?php echo($row['precio']); ?>" value="<?php echo($row['descripcion']); ?>"><?php echo($row['descripcion']); ?></option><?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" >
                          <label for="l_cantidad">Cantidad</label><br>
                          <input type="number" class="form-control cantidad_prod" autocomplete="off" id="cantidad" name="cantidad" size="1" value="1" onkeyup="get_pre_subtotal(this.value)" required >
                        </div>
                        <div class="form-group">
                          <label for="l_precio_u">Precio Unt.</label><br>
                          <div class="form-control precio_u" id="precio_u" name="precio_u"></div>
                        </div>
                        <div class="form-group">
                          <label for="l_subtotal">Subtotal</label><br>
                          <div class="form-control subtotal_ped" id="subtotal" name="subtotal"></div>
                        </div>
                        <div class="form-group">
                          <br>
                          <button type="submit" class="btn btn-primary agregar_prod" onclick="agregar_pedido()">Agregar</button>
                        </div>
                      </div>

                      <div class="col-xs-12" style="padding:0;margin-left:0;padding-right:1em;"><!--TERCERA SECCION DE PEDIDOS -->
                        <div id="precarga" style="margin-left:auto; margin-right:auto;"></div> <!-- TABLA PARA MOSTRAR CONTENIDO DEL PEDIDO -->
                          <div class="panel panel-default">
                            <table class="table table-striped" id="t_pedido">
                              <tr id="t_encabezado">
                                <td><h4><b>Descripcion Producto</h4></b></td>
                                <td><h4><b>Cantidad</h4></b></td>
                                <td><h4><b>Valor Unt.</h4></b></td>
                                <td><h4><b>Subtotal</h4></b></td>
                                <td><h4><b>Retirar</h4></b></td>
                              </tr>
                              <tbody id="body_t_pedido">
                              </tbody>
                              <tr>
                                <td></td><td></td><td><h4><b>Total:</h4></b></td>
                                <td id="total_id"><h3>---</h3></td>
                                <td><button type="button" class="btn btn-success enviar_ped" onclick="ingresar_pedido()">Enviar</button></td>
                              </tr>
                          </table>
                      </div>
                      <div id="resultado"></div>
                        <input type="hidden" id="id_pro">
                      </div>
                    </div>
                    <div class="col-xs-3 col-md-2" style="text-align:right; padding-left:2em;"><a href="pedidos.php"><button type="button" class="btn btn-danger btn-lg btn-block">Atras</button></a></div> 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <?php include 'footer.php';
      }
      else{
        header("Location:pedidos.php"); 
      }
    ?>
    </div>
  </div>