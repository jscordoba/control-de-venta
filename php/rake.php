<?php 
	include 'conn.php';
	date_default_timezone_set('America/Bogota');
	$fecha_pago	= date("Y-m-d");
	$datos 		=array();
	if ($_FILES) {
		foreach ($_FILES as $vAdjunto) {
			if ($vAdjunto["size"] > 0){
				$row = 1;
				if (($handle = fopen($vAdjunto["tmp_name"], "r")) !== FALSE) {
				    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				    	$num = count($data);
				    	if ($row > 1 ) {
					        for ($c=0; $c < $num; $c++) {
					        	$dataCell = explode(";", $data[$c]);

					        	$pago = $dataCell[1];
					        	$cedula = $dataCell[0];
					        	$nota = $dataCell[2];

					        	if ($cedula != '' && $pago != '') {
					           		$datos[$cedula] = array($pago,$nota);
					        	} 	
					        }
				        }
				        $row++;
				    }
				    $values_pagos = "";
			   		$values_saldos = "";
				    foreach ($datos as $key => $value) {
			    		$sql_saldos_actuales = "SELECT t_saldo_valor, funcionarios_id_funcionario 
												FROM `t_saldo` 
												WHERE `idt_saldo` = (
												SELECT MAX(`idt_saldo`) FROM t_saldo 
												where funcionarios_id_funcionario = ". $key."
												GROUP BY `funcionarios_id_funcionario` 
												)";
			    		$sql_saldos_actuales = mysqli_query($conn,$sql_saldos_actuales); 
			    		$saldo = mysqli_fetch_array($sql_saldos_actuales);

			    		if ( isset($datos[$saldo['funcionarios_id_funcionario']][0]) && $saldo['t_saldo_valor'] >= $datos[$saldo['funcionarios_id_funcionario']][0]) {
				    		$restante =  $saldo['t_saldo_valor'] - $datos[$saldo['funcionarios_id_funcionario']][0];
				    		$values_pagos .= "(".$saldo['funcionarios_id_funcionario'].",'".$fecha_pago."','".$datos[$saldo['funcionarios_id_funcionario']][0]."','".$datos[$saldo['funcionarios_id_funcionario']][1]."'),";
			    			$values_saldos .= "('".$restante."', '".date('Y-m-d')."', '".$saldo['funcionarios_id_funcionario']."'),";
				    	}else{
				    		unset($datos[$saldo['funcionarios_id_funcionario']]);
				    	}
			    	}
			    	$restante = 0;
			    	$values_pagos = substr($values_pagos, 0, -1);
				   	$values_saldos = substr($values_saldos, 0, -1);
				    $sql_pagos = "INSERT INTO pagos (id_funcionario, fecha, valor_pago, observacion) VALUES $values_pagos";
				    $sql_pagos = mysqli_query($conn,$sql_pagos);
				    if ( $sql_pagos == true) {
				    	$sql_saldos = "INSERT INTO t_saldo (t_saldo_valor,t_saldo_fecha, funcionarios_id_funcionario) VALUES $values_saldos";
				    	$sql_saldos = mysqli_query($conn,$sql_saldos);
				    }else{
				    	$sql_saldos = false;
				    }
				    $data['return'] 	= array(($sql_pagos) ? 1 : 0,($sql_saldos) ? 1 : 0);
				    $data['updates'] 	= array($datos);
				    echo json_encode($data);
				    fclose($handle);			 
				}
			}
		}
	}else{
		echo "el sistema no a recibido el documento";
	}
?>