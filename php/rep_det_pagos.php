<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}else{ 
  include 'conn.php';
  date_default_timezone_set('America/Bogota');
  $fecha_pag=date("Y-m-d");

  /*$resultado = mysqli_query ($conn,"SELECT p.id_pago, p.fecha, p.id_funcionario, f.nombres, f.apellidos, p.valor_pago, fd.deuda
                                    FROM pagos p
                                    LEFT JOIN funcionarios f ON ( p.id_funcionario = f.id_funcionario ) 
                                    LEFT JOIN funcionarios_deben fd ON ( f.id_funcionario = fd.id_funcionario)");// ORIGEN DE DATOS*/

  $resultado = mysqli_query ($conn,"SELECT p.id_pago, p.fecha, p.id_funcionario, f.nombres, f.apellidos, p.valor_pago,t_saldo.t_saldo_valor 
                                      FROM funcionarios f 
                                        INNER JOIN (
                                            SELECT max(ts.idt_saldo) as idt,ts.funcionarios_id_funcionario
                                            FROM t_saldo ts 
                                            WHERE ts.t_saldo_valor>0
                                            GROUP BY ts.funcionarios_id_funcionario
                                        ) as saldo ON(f.id_funcionario=saldo.funcionarios_id_funcionario)
                                        INNER JOIN ( t_saldo ) ON (t_saldo.idt_saldo=saldo.idt)
                                        INNER JOIN pagos p ON ( p.id_funcionario = f.id_funcionario )");
  
  $registros = mysqli_num_rows ($resultado);

  if ($registros) {
   require_once '../lib/Classes/PHPExcel.php';
   $objPHPExcel = new PHPExcel();

    $objPHPExcel->getActiveSheet()
      ->setTitle("Rep_Detalle_Pedidos") // ESTABLECER NOMBRE A LA HOJA DE EXCEL
      ->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//ALINEAR TEXTO

   //Informacion del excel
   $objPHPExcel->
    getProperties() //PROPIEDADES DEL DOCUMENTO
        ->setCreator("Jeison Cordoba")
        ->setLastModifiedBy("Ingeniero TIC - Magnetron S.A.S")
        ->setTitle("Reportes desde BD")
        ->setSubject("Reportes")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("Magnetorn.com.co  con  phpexcel")
        ->setCategory("CDMAG");        

       $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1','REPORTE DETALLES DE PAGOS A LA FECHA '.$fecha_pag) //ENCABEZADOS
        ->setCellValue('A2','ID Pago')
        ->setCellValue('B2','Identificacion')
        ->setCellValue('C2','Nombres')
        ->setCellValue('D2','Apellidos')
        ->setCellValue('E2','Fecha')
        ->setCellValue('F2','Valor')
        ->setCellValue('G2','Pendiente')
        ->setAutoFilter("A2:G2")
        ->getStyle("A1:G2")->applyFromArray(array("font" => array( "bold" => true))); //FORMATOS DE TEXTO "BOLD" negrita

        $objPHPExcel->getActiveSheet()
        ->mergeCells('A1:G1') //COMBINAR CELDAS
        ->getRowDimension('1')->setRowHeight(30);//REDIMENSIONAR ALTO DE FILA
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); //REDIMENSIONAR ANCHO DE COLUMNAS
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
    
    $i = 3; 
    
    while ($registro = mysqli_fetch_object($resultado)) { //RELLENADO DE CELDAS
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, $registro->id_pago)
        ->setCellValue('B'.$i, $registro->id_funcionario)
        ->setCellValue('C'.$i, $registro->nombres)
        ->setCellValue('D'.$i, $registro->apellidos)
        ->setCellValue('E'.$i, $registro->fecha)
        ->setCellValue('F'.$i, $registro->valor_pago)
        ->setCellValue('G'.$i, $registro->t_saldo_valor);
      $i++;
   }
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Rep_Detalle_Pagos_Hist.xls"');//NOMBRE DEL DOCUMENTO Y FORMATO
    header('Cache-Control: max-age=0');

    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    exit;
    mysqli_close ();
  }
    echo "<script>";
    echo "alert('--- Sin informacion que mostrar  ---')"; //muestra mensaje de error
    echo "</script>";
    echo "<meta http-equiv='Refresh' content='0; url=reportes.php'>"; //redireccionamos a la página
}
?>