<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}else{ 
  include 'conn.php';
  date_default_timezone_set('America/Bogota');
  $fecha_pag=date("Y-m-d");
  $ini = addslashes($_REQUEST['ini']);
  $fin = addslashes($_REQUEST['fin']);
  $area = (isset($_REQUEST['area']) && $_REQUEST['area'] != '') ?  "= '".$_REQUEST['area']."'" : " LIKE '%%' ";

  $sql = "SELECT p.id_pago, p.fecha, p.id_funcionario, f.nombres, f.apellidos, p.valor_pago,t_saldo.t_saldo_valor 
                                      FROM funcionarios f 
                                        INNER JOIN (
                                            SELECT max(ts.idt_saldo) as idt,ts.funcionarios_id_funcionario
                                            FROM t_saldo ts 
                                            WHERE ts.t_saldo_valor>0
                                            GROUP BY ts.funcionarios_id_funcionario
                                        ) as saldo ON(f.id_funcionario=saldo.funcionarios_id_funcionario)
                                        INNER JOIN ( t_saldo ) ON (t_saldo.idt_saldo=saldo.idt)
                                        INNER JOIN pagos p ON ( p.id_funcionario = f.id_funcionario )
                                        WHERE p.fecha BETWEEN '$ini' AND '$fin' AND f.area $area";
  $resultado = mysqli_query ($conn,$sql);
  
  $registros = mysqli_num_rows ($resultado);

  if ($registros) {
    $delimiter = ",";
    $filename = "pagos_"  . $_REQUEST['ini'] ."-".$_REQUEST['fin']. ".csv";

    $f = fopen('php://memory', 'w');

    $fields = array('ID Pago', 'Identificacion', 'Nombres', 'Apellidos', 'Fecha', 'Valor', 'Pendiente');
    fputcsv($f, $fields);

    while($registro = $resultado->fetch_assoc()) {
      fputcsv($f, $registro);
    }

    fseek($f, 0);

    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');

    fpassthru($f);
    exit;
    mysqli_close ();
  }
    echo "<script>";
    echo "alert('--- Sin informacion que mostrar  ---')"; //muestra mensaje de error
    echo "</script>";
    echo "<meta http-equiv='Refresh' content='0; url=reportes.php'>"; //redireccionamos a la página
}
?>