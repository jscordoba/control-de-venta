<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}else{ 
  include 'conn.php';
  date_default_timezone_set('America/Bogota');
  $planta = $_REQUEST['planta'];
  $ini = $_REQUEST['ini'];
  $fin = $_REQUEST['fin'];
  $area = (isset($_REQUEST['area']) && $_REQUEST['area'] != '' ) ?  "= '".$_REQUEST['area']."'" : " LIKE '%%' ";

  $fecha_busq=date("Y-m-d");
  $fecha_busq;
  //$fecha_ver=date("M-d");

  $sql = "SELECT p.id_pedido,pr.descripcion,pp.precio_unidad,pp.cantidad,p.id_funcionario,f.nombres,f.apellidos,p.fecha
                                  FROM productos pr 
                                  RIGHT JOIN pedido_producto pp ON(pr.id_producto=pp.id_producto) 
                                  JOIN pedidos p ON(pp.id_pedido=p.id_pedido) 
                                  JOIN funcionarios f ON(p.id_funcionario=f.id_funcionario)
                                  WHERE p.fecha BETWEEN  '$ini' AND '$fin' AND f.area $area
                                  AND p.pedido_planta = '$planta'
                            ";
  $resultado = mysqli_query ($conn,$sql);//ORIGEN DE DATOS

  $registros = mysqli_num_rows ($resultado);
    if ($registros > 0) {
      $delimiter = ",";
      $filename = "pedidos_".$planta."_". $_REQUEST['ini']."_".$_REQUEST['fin'].".csv";
      
      $f = fopen('php://memory', 'w');
      
      $fields = array('ID pedido', 'Descripcion', 'Precio', 'Unidades', 'Identificacion', 'Nombres', 'Apellidos', 'Fecha');
      fputcsv($f, $fields);
      
      while($registro = $resultado->fetch_assoc()) {
          fputcsv($f, $registro);
      }
      
      fseek($f, 0);
      
      header('Content-Type: text/csv');
      header('Content-Disposition: attachment; filename="' . $filename . '";');
      
      fpassthru($f);
      exit;
      
      mysqli_close ();
    }
  
    echo "<script>";
    echo "alert('--- Sin informacion que mostrar  ---')"; //muestra mensaje de error
    echo "</script>";
    echo "<meta http-equiv='Refresh' content='0; url=reportes.php'>"; //redireccionamos a la página]
}
?>