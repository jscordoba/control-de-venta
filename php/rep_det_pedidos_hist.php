<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}else{ 
  include 'conn.php';
  date_default_timezone_set('America/Bogota');
  //$fecha_busq=date("Y-m-d");
  //$fecha_ver=date("M-d");
  $planta = $_REQUEST['planta'];

  $resultado = mysqli_query ($conn,"SELECT p.id_pedido,pr.descripcion,pp.precio_unidad,pp.cantidad,p.id_funcionario,f.nombres,f.apellidos,p.fecha
                              FROM productos pr RIGHT JOIN pedido_producto pp ON(pr.id_producto=pp.id_producto) 
                              JOIN pedidos p ON(pp.id_pedido=p.id_pedido) 
                              JOIN funcionarios f ON(p.id_funcionario=f.id_funcionario)
                              WHERE p.pedido_planta = '".$planta."'");//ORIGEN DE DATOS
  
  $registros = mysqli_num_rows ($resultado);

  if ($registros > 0) {
   require_once '../lib/Classes/PHPExcel.php';
   $objPHPExcel = new PHPExcel();

    $objPHPExcel->getActiveSheet()
      ->setTitle("Rep_Detalle_Pedidos") // ESTABLECER NOMBRE A LA HOJA DE EXCEL
      ->getStyle("A1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);//ALINEAR TEXTO

   //Informacion del excel
   $objPHPExcel->
    getProperties() //PROPIEDADES DEL DOCUMENTO
        ->setCreator("Jeison Cordoba")
        ->setLastModifiedBy("Ingeniero TIC - Magnetron S.A.S")
        ->setTitle("Reportes desde BD")
        ->setSubject("Reportes")
        ->setDescription("Documento generado con PHPExcel")
        ->setKeywords("Magnetorn.com.co  con  phpexcel")
        ->setCategory("CDMAG");  

    $i = 3;  
       $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1','REPORTE DETALLES DE PEDIDOS HISTORICO') //ENCABEZADOS
        ->setCellValue('A2','ID Pedido')
        ->setCellValue('B2','Descripcion de Productos')
        ->setCellValue('C2','Precio')
        ->setCellValue('D2','Unidades')
        ->setCellValue('E2','Identificacion')
        ->setCellValue('F2','Nombres')
        ->setCellValue('G2','Apellidos')
        ->setCellValue('H2','Fecha')
        ->setAutoFilter("A2:H2")
        ->getStyle("A1:H2")->applyFromArray(array("font" => array( "bold" => true))); //FORMATOS DE TEXTO "BOLD" negrita

        $objPHPExcel->getActiveSheet()
        ->mergeCells('A1:H1') //COMBINAR CELDAS
        ->getRowDimension('1')->setRowHeight(30);//REDIMENSIONAR ALTO DE FILA
        
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); //REDIMENSIONAR ANCHO DE COLUMNAS
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('6')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

    while ($registro = mysqli_fetch_object($resultado)) { //RELLENADO DE CELDAS
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, $registro->id_pedido)
        ->setCellValue('B'.$i, $registro->descripcion)
        ->setCellValue('C'.$i, $registro->precio_unidad)
        ->setCellValue('D'.$i, $registro->cantidad)
        ->setCellValue('E'.$i, $registro->id_funcionario)
        ->setCellValue('F'.$i, $registro->nombres)
        ->setCellValue('G'.$i, $registro->apellidos)
        ->setCellValue('H'.$i, $registro->fecha);
      $i++;
   }
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Rep_Detalle_Pedidos_Hist.xls"');//NOMBRE DEL DOCUMENTO Y FORMATO
    header('Cache-Control: max-age=0');

    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    exit;
    mysqli_close ();
  }

    echo "<script>";
    echo "alert('--- Sin informacion que mostrar  ---')"; //muestra mensaje de error
    echo "</script>";
    echo "<meta http-equiv='Refresh' content='0; url=reportes.php'>"; //redireccionamos a la página
}
?>