<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}else{ 
?>
<!DOCTYPE HTML>
	<html>
	<head>
		<title>FodeMag - Opciones</title>
		<?php
			include 'header.php';
			include 'conn.php';
			date_default_timezone_set('America/Bogota');

		?>

	<script type="text/javascript">
		 $(document).ready( function() {

			$(".rep_det_pedidos").click(function(event) {
				/* Act on the event */
				var rep_planta 	= 	$("#planta").val();
				var fecha_ini 	=	$("#fecha_ini").val();
				var fecha_fin 	= 	$("#fecha_fin").val();
				var rep_area 	= 	$("#rep_area").val();

				if(rep_planta==="" || rep_planta===null){
					alert("No se a seleccionado una Planta.\nIntentelo de nuevo.");
				}else if(fecha_ini == '' || fecha_fin == ''){
					alert("Ingresa el rango de fechas");
				}else{
					var url = "rep_det_pedidos.php?planta="+rep_planta+"&ini="+fecha_ini+"&fin="+fecha_fin+"&area="+rep_area;
					url = encodeURI(url);
					console.log(url);
					$( location ).attr("href", url);
				}
			});

			$(".rep_det_pagos").click(function(event) {
				/* Act on the event */
				var rep_planta 	= 	$("#planta").val();
				var fecha_ini 	=	$("#fecha_ini").val();
				var fecha_fin 	= 	$("#fecha_fin").val();
				var rep_area 	= 	$("#rep_area").val();

				if(fecha_ini == '' || fecha_fin == ''){
					alert("Ingresa el rango de fechas");
				}else{
					var url = "rep_det_pagos_dia.php?planta="+rep_planta+"&ini="+fecha_ini+"&fin="+fecha_fin+"&area="+rep_area;
					url = encodeURI(url);
					$( location ).attr("href", url);

				}

			});

			$(".rep_det_pedidos_hist").click(function(event) {
				/* Act on the event */
				var rep_planta = $("#planta").val();

				if (rep_planta==="" || rep_planta===null) {
					alert("No se a seleccionado una Planta.\nIntentelo de nuevo.");
				}else if(fecha_ini == '' || fecha_fin == '' ){
					alert("Ingresa el rango de fechas");
				}else{
					alert(rep_planta);
					$( location ).attr("href", "rep_det_pedidos_hist.php?planta="+rep_planta);
				}
			});

			

			$(".rep_det_pagos_hist").click(function(event) {
				/* Act on the event */
				 var rep_planta = $("#planta").val();

				// if(rep_planta==="" || rep_planta===null){
				// 	alert("No se a seleccionado una Planta.\nIntentelo de nuevo.");
				// }else{
				// 	alert(rep_planta);
				// 	$( location ).attr("href", "rep_det_pagos.php?planta="+rep_planta);
				// }

				$( location ).attr("href", "rep_det_pagos.php?planta="+rep_planta);

			});

		});
	</script>

	</head>
	<body>
		<div class="total-content" style="vertical-align:middle; margin: 0; text-align: center;">
			<div class="row" style="margin: 0; text-align: center;">
                <div class="col-xs-1"></div>
				<div class="col-xs-10" style="margin: 0; text-align: center;">

                 <div class="container" style="margin-top:5%;">
						<div class="row">
							<div class="col-xs-3"></div>
							<div class="col-xs-6" style="vertical-align:middle;">
								<div class="col-xs-12 centerConetnt">
									<div>
										<label>Fecha Inicial</label>
										<input name="fecha_ini" id="fecha_ini" type="date" required class="form-control">
									</div>
									<div class="">
										<label>Fecha Final</label>
										<input name="fecha_ini" id="fecha_fin" type="date" required class="form-control">
									</div>

								</div>
								
								<?php 
								 $result=mysqli_query($conn, "SELECT * FROM funcionarios WHERE estado = true GROUP BY area ORDER BY area ASC");  ?>

								 	<div class="col-xs-12">
									 	<div>
										 	<label>Seleccionar area</label>
											<select name="planta" id="rep_area" required class="form-control">
											<option value="">Seleccionar el area...</option>
											<?php  while ($areas=mysqli_fetch_array($result)) { ?>
												<option value="<?= $areas['area'] ?>"><?= $areas['area'] ?></option>
								           	<?php } ?>
								           </select>
							           </div>
						           </div>

						        <div class="col-xs-12">
									<label>Seleccionar planta</label>
									<select name="planta" id="planta" required class="form-control">
										<option value="">Seleccionar Planta...</option>
										<option value="PL1">PLANTA 1</option>
										<option value="PL2">PLANTA 2</option>
									</select>
								</div>
								 

								<?php if ($_SESSION['id_sesion']=='venta') { 
									header("Location:index.php");
								}
								if ($_SESSION['id_sesion']=='fodemag' OR $_SESSION['id_sesion']=='admin') { ?>
									<div class="col-xs-12" style="margin-top: 1em;"><button type="button" class="btn btn-primary btn-lg btn-block rep_det_pedidos">Historial de pedidos</button></div>
									<div class="col-xs-12" style="margin-top: 1em; display: none"><button type="button" class="btn btn-default btn-lg btn-block rep_det_pedidos_hist">Pedidos Histórico</button></div>
									<div class="col-xs-12" style="margin-top: 1em;"><button type="button" class="btn btn-success btn-lg btn-block rep_det_pagos">Historial de pagos</button></div>
									<div class="col-xs-12" style="margin-top: 1em; display: none"><button type="button" class="btn btn-success btn-lg btn-block rep_det_pagos_hist">Pagos Histórico</button></div>
									<!--<div class="col-xs-12" style="margin-top: 1em;"><a href="rep_det_pagos.php?fecha='1'"><button type="button" class="btn btn-default btn-lg btn-block rep_det_pagos_fecha">Saldo por Fecha</button></a></div>-->
									<div class="col-xs-12" style="margin-top: 1em;"><a href="pedidos.php"><button type="button" class="btn btn-default btn-lg btn-block">Volver</button></a></div>
								<?php } ?>
							</div>
							<div class="col-xs-3 col-sm-1 col-lg-3"></div>
						</div>
					</div><!-- -->

                    <div class="col-xs-1"></div>

					
				</div>
			</div>

		    <!--<div class="row" id="form_print" style="display: none; text-align:center;">
		        <form action="../lib/ficheroExcel.php" method="post" id="FormularioExportacion" class="form-inline">
		            <div class="modal-header">
		                <h3 align="center">Reporte para Detalles de pedidos</h3>
		            </div>
		            <div align="center">
		                <input type="text" name="nombre" class="form-control" autocomplete="off" placeholder="Nombre del Fichero" value="CDMAG-Deben<?php echo $fecha; ?>" required><br>
		                <strong>Imprimir en: </strong><br>
		                <select name="imp" class="form-control">
		                    <option value="excel">Hoja de Excel</option>
		                </select>
		                <br><br>
		                <button type="submit" class="btn botonExcel"><i class="icon-print"></i> <strong>Imprimir Reporte</strong></button>
		                <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
		            </div>
		        </form>
		        <div class="col-sm-offset-3 col-xs-2 col-md-2 col-lg-2 col-sm-3 "><a href="ing_pag.php"><button style="width:5em; height:3em;" type="button" class="btn btn-default btn-lg btn-block">Atras</button></a></div>
		    </div>-->
		<?php include 'footer.php' ?>
		</div>
	</body>
	</html>
	<?php
	} 
?>