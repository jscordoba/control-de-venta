<?php  
    session_start();
    include 'conn.php';
    include 'enviar_correo.php';
    date_default_timezone_set('America/Bogota');

    if (isset($_REQUEST["id_ped"]) || isset( $_REQUEST["idPedido"])  ) {
        if (isset($_REQUEST["paso"]) && $_REQUEST["paso"] == 'delete') {
            $data_user = mysqli_query($conn,"SELECT funcionarios.id_funcionario, funcionarios.fun_email FROM log_delete_producto INNER JOIN funcionarios ON  log_delete_producto.id_funcionario = funcionarios.id_funcionario WHERE log_delete_producto.id_pedido = ".$_REQUEST["id_ped"]." GROUP BY funcionarios.id_funcionario LIMIT 1");
            $row_data_user  = mysqli_fetch_array($data_user,MYSQLI_BOTH);
            enviar_correo( $row_data_user[0],$_REQUEST["valor_ped"],$row_data_user[1],$_REQUEST["id_ped"], 'delete');
        }else{
            $id_productos=$_REQUEST["ides"];
            $cantidades=$_REQUEST["cantidades"];
            $valor_pedido=$_REQUEST["valor"];
            $id_user = $_REQUEST["id_user"];
            $valor_compras = $_REQUEST["valor_compras"];
            $id = $_REQUEST["idPedido"];

            $q_email=mysqli_query($conn,"SELECT fun_email FROM funcionarios WHERE id_funcionario='".$id_user."'"); 
            $row_email=mysqli_fetch_array($q_email,MYSQLI_BOTH);

            enviar_correo($id_user,$valor_pedido,$row_email[0],$id); //enviar notificacion a correo
        }
    }
    
?>