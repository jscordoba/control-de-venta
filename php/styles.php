
<!-- ESTILOS Y FUENTES Y SCRIPTS COMUNES PARA EL APLICATIVO-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="../css/bootstrap.css" rel='stylesheet' type='text/css' media="all" />
	<!-- Custom Theme files -->
	<link href="../css/style.css" rel='stylesheet' type='text/css' media="all" />
	<script src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery-1.11.3.min.js"></script>
	<!--<script type="text/javascript" src="../js/styles.js"></script>--><!-- STYLES JS -->

	<!--webfonts-->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<!--webfonts-->	

    
    
    <head>
    	<script type="text/javascript">
    	jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});

			$("#inserta_usuario").submit(function(event) {
				event.preventDefault();
				url = $(this).attr('action');
				var formElement = document.getElementById("inserta_usuario");
				formData = new FormData(formElement);
				formData.append("opc", 'ins');
				$.ajax({
						beforeSend: function( xhr ) {
		                    $('#precarga').addClass('text-center').removeClass('hide').html("<i class='fa fa-spinner fa-spin size2' style='font-size: 4em'></i>");
		                },
						url: url,
						type: "POST",
						data: formData,
						processData: false,  // tell jQuery not to process the data
						contentType: false ,  // tell jQuery not to set contentType
						success: function( data ) {
							if (data == "Registro de usuario exitoso") {
								$('#precarga').addClass('text-center').removeClass('hide').html("<i class='fas fa-check-circle green size2' style='font-size: 4em'></i>");
								$("#message").text(data).addClass('green').removeClass('hide');
							}else{
								$('#precarga').addClass('text-center').removeClass('hide').html("<i class='fas fa-exclamation red size2' style='font-size: 4em'></i>");
								$("#message").text(data).addClass('red').removeClass('hide');
							}
		                    
		                }
					});
			});
			$(".update_pass_user").submit(function(event) {
				event.preventDefault();

				var formElement = this;
				formData = new FormData(formElement);
				$.ajax({
						beforeSend: function( xhr ) {
		                    $('#precarga').addClass('text-center').removeClass('hide').html("<i class='fa fa-spinner fa-spin size2' style='font-size: 4em'></i>");
		                },
						url: "update_user.php",
						type: "POST",
						data: formData,
						processData: false,  // tell jQuery not to process the data
						contentType: false ,  // tell jQuery not to set contentType
						success: function( data ) {
							if (data == 1) {
								$('#precarga').addClass('text-center').removeClass('hide').html("<i class='fas fa-check-circle green size2' style='font-size: 4em'></i>");
								$("#message").text('Contraseña actualizada con exito').addClass('green').removeClass('hide');
							}else{
								$('#precarga').addClass('text-center').removeClass('hide').html("<i class='fas fa-exclamation red size2' style='font-size: 4em'></i>");
								$("#message").text("Error al actualizar la contraseña").addClass('red').removeClass('hide');
							}							
		                }
					});
			});			
		});
		
		// AGREGAR PRODUCTOS AL PEDIDO EN LA TABLA -->
		function agregar_pedido() {
			var cantidad = document.getElementsByName("cantidad")[0].value;
			var subtotal = document.getElementById("subtotal").innerText;
			if (parseInt(cantidad) >= 1 && parseInt(subtotal) >= 1 ) {
		        var id_producto=document.getElementById("id_pro").value;
		        var cedula=document.getElementById("list_funcionarios").value;
		        var descripcion = document.getElementsByName("producto")[0].value;
		        var cantidad = document.getElementsByName("cantidad")[0].value;
		        var valor_u = document.getElementById("precio_u").innerText;
		       
		        var id_producto=$("#id_pro").val();
			    var descripcion = document.getElementsByName("producto")[0].value;
			    
			    var valor_u = $("#precio_u").html();
			    var subtotal = $("#subtotal").html();

		        var num_item = document.getElementsByTagName("td").length;
		        var retirar  = "<img class=\"eliminar_pedido\" src=\"../images/icon_svg/delete84.svg\" width=\"20em\" height=\"20em\" title=\"Eliminar del Pedido\">";

		        var table = $("#body_t_pedido").append("<tr class=\"prod_ped\"><td>"+descripcion+"</td><td>"+cantidad+"</td><td>"+valor_u+"</td><td class='subtotal_pro'>"+subtotal+"</td><td>"+retirar+"</td><td style='display:none' class='valores_producto'>"+id_producto+"|"+cantidad+"|"+valor_u+"|"+descripcion+"|"+cedula
		        	+"</td></tr>");

		        $(".list_funcionarios").attr("disabled",true);
		        $(".list_areas").attr("disabled",true);
		        $(".enviar_ped").attr("disabled", false);
		        $(".cantidad_prod").attr("value",1);
		        $(".cantidad_prod").attr("disabled", true);
		        $('.desc_prod option:eq(0)').prop('selected', true);
		        $('#search_producto').val('');
		        $(".precio_u").html("");
		        //$(".desc_prod").prepend('<option disabled selected="">Selecciona el producto</option>');
		        calcula_total();
	        }else{
	        	alert('La cantidad ingresada no es valida');
	        }
		}
	    

	// PRECIO DE PRODUCTO -->
		
	    function get_precio(str, costo = 0, id_producto = 0) {
	        if (str=="") {
	            $("#precio_u").html()="";
	            return;
	        }
	        else{
	        	$.post("get_pre.php",{p_u:str},function(datos,status){
	                //alert(datos);
	                var valores = datos.split("|");
	                //alert(valores[0]);
	                $("#precio_u").html(valores[0]);
	                $("#id_pro").val(valores[1]);
	                //$(".cantidad_prod").attr("value","");
	                $(".subtotal_ped").html("");
	                $(".agregar_prod").attr("disabled", true);
	                //calcula_total();
	                get_pre_subtotal(1);
	            });
	        }
	    }
	    

	// BUSQUEDA DE FUNCIONADIO -->
	    
        function get_funcionario(str) {
            if (!str.length) {
                $("#nombre").html("...");
                 $("#usuarioSeleccionado").val("");
                return;
            } else{
                $.post("get_us.php",{id_u:str},function(response){
                    $("#nombre").html("");

                    if (response!="") {                        
                         $("#nombre").html(response);
                          var datos = response.split("-");
                          $("#nombre").html(datos[0]);
                          $("#usuarioSeleccionado").val(datos[1]);
                    }
                });
            }
        }

	// BUSQUEDA DE FUNCIONADIOS PARA PEDIDOS -->
	    
        function get_funcionarios(str, name = "") {
        	$("#id_usuario").val("");
	        if (!str.length) {
	            $("#list_funcionarios").html("Seleccione el área");
	                return;
	            } else{
	            $.post("get_us.php",{id_u:str,opc:1, name: name},function(response){

	                if (response!="") {
	                    if ($("#list_funcionarios").html()=="Selecione el área") {
	                        $(".desc_prod").attr("disabled", true);
	                        $(".view_filtro_user").addClass('hide');
	                    }else{
	                        $(".desc_prod").attr("disabled", true);
	                        $(".view_filtro_user").removeClass('hide');
	                        
	                        var nombres=response.split("|");
	                        $("#list_funcionarios").html("");
	                       
	                        for (var i = 0; i < nombres.length; i++) {
	                            var nom_cedu=nombres[i].split("-");
	                            
	                            for (var x = 0; x < (nom_cedu.length-1); x++) {
	                                //alert(nom_cedu[0]);
	                                $("#list_funcionarios").append("<option value='"+nom_cedu[0]+"'>"+nom_cedu[1]+"</option>");
	                                //break;
	                            }
	                        };
	                        if (nombres.length == 2) {
	                        	var id = nombres[0].split("-");
	                        	$('#id_usuario').val(id[0]);
	                        	$(".desc_prod").attr("disabled",false);
	                        }else{
	                        	 $("#list_funcionarios").prepend('<option disabled selected="">Seleccione el funcionario</option>');
	                        }
	                    }
	                }
	                else{
	                    $(".desc_prod").attr("disabled", true);
	                    $(".cantidad_prod").attr("disabled", true);
	                    $(".agregar_prod").attr("disabled", true);

	                    $(".desc_prod").attr("value","Selecciona el producto").attr('disabled', true);
	                    $(".cantidad_prod").attr("value","");
	                    $(".precio_u").html("");
	                    $(".subtotal_ped").html("");
	                };
	            });
	        }
	    }
	   
	    function get_id_usuario(und) {
	        var id_usuario=und;
	        $("#id_usuario").val(id_usuario);
	    }
	    

	// PRECIO DE SUBTOTAL -->
	    
	    function get_pre_subtotal(und) {
	        var patron = /^\d*$/; //Expresión regular para aceptar solo números enteros

	        if(und=="" || und=="0"){
	            $(".agregar_prod").attr("disabled",true);
	            var estado=document.getElementById("subtotal").innerHTML="NaN";
	        }else if (und=="NaN"){
	            $(".agregar_prod").attr("disabled",true);
	            var estado=document.getElementById("subtotal").innerHTML="NaN";
	        }else if(!patron.test(und)){
	            $(".agregar_prod").attr("disabled",true);
	            var estado=document.getElementById("subtotal").innerHTML="NaN";
	        }else{
	            var precio_u = parseInt(document.getElementById("precio_u").innerText);
	            var c_subtotal=precio_u*parseInt(und);
	            var estado=document.getElementById("subtotal").innerHTML=c_subtotal;
	            $(".agregar_prod").attr("disabled",false);
	        }
	    }
	    


	// PRECIO DE TOTAL PARA LA VENTA DE PEDIDO-->
	    
	    function calcula_total() {
	        var suma=0;
	        $(".subtotal_pro").each(function(index){
	           var valor=$(this).html();
	           suma+=parseInt(valor);
	        });

	        $("#total_id").html(suma);
	        //$("#cantidad").val("");
	        $("#subtotal").html("");
	        
	        btn_enviar();
	        $(".agregar_prod").attr("disabled",true);
	    }
	    

	// PRECIO DE TOTAL PARA LA VISTA DE FUNCIONARIOS DEUDORES-->
	    
        function calcula_total_fun() {
            var suma=0;
            $(".subtotal_ped_fun").each(function(index){
                var valor=$(this).html();
                suma+=parseInt(valor);
            });

            $("#total_ped_fun").html('$' + parseFloat(suma, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
        }
	    

	    // PRECIO DE TOTAL PARA LA VISTA DE FUNCIONARIOS DEUDORES POR BUSQUEDA-->
	    
        function calcula_total_fun_busq() {
            var suma_b=0;
            $(".subtotal_ped_fun_busq").each(function(index){
                var valor_b=$(this).html();
                suma_b+=parseInt(valor_b);
            });

            $("#total_ped_fun_busq").html('$' + parseFloat(suma_b, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            $("#lista_pago_func").css("display","none");
        }
    

    
        function calcula_total_ped() {
            var suma_b=0;
            $(".calcula_total_ped").each(function(index){
                var valor_b=$(this).html();
                suma_b+=parseInt(valor_b);
            });

            $("#total_ped_fun_busq").html('$' + parseFloat(suma_b, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            $("#lista_pago_func").css("display","none");
        }
	    

	    // INGRESAR PEDIDO DEFINITIVO DEL USUARIO -->

	    
        function ingresar_pedido() {
            $(".enviar_ped").attr('disabled',true);
            var ids=new Array();
            var cantidad=new Array();
            var valor_compras=new Array();
            var descripciones =new Array();
            var cedulas =new Array();
            var a=0;
            $(".valores_producto").each(function(){
               var valores=$(this).html().split("|");
               ids[a]=valores[0];
               cantidad[a]=valores[1];
               valor_compras[a]=valores[2];
               descripciones[a] = valores[3];
               a++;
            });

            var x_ides=ids.join(",");
            var x_cantidad=cantidad.join(",");
            var x_valor=$("#total_id").html();
            var x_id_user=$("#id_usuario").val();

            if (x_id_user != '') {
	            var dataPedido = {descripciones: descripciones ,ides:x_ides, cantidades: x_cantidad, valor: x_valor, id_user: x_id_user, valor_compras: valor_compras};
	            $.ajax({
				  	type: "POST",
				  	url: "ins_ped.php",
				  	data: dataPedido,
				  	beforeSend: function( xhr ) {
	                    //$('#precarga').addClass('text-center').html("<i class='fa fa-spinner fa-spin size2' style='font-size: 4em'></i>");
	                },
				  	success: function(data){
				  		$(".enviar_ped").attr("disabled", true);

			            var id = data.split(",");
			            var id = parseInt(id[0]);
						dataPedido.idPedido = id;
				        if (id > 0) {
				        	send_mail(dataPedido);
				        	limpiar_pedido();
	                    	alert("Se ha registrado la compra con éxito.");
	                    	location.reload();
		                }else{
		                    alert("Error al Registrar el Pedido");
		                    addLocalStorage(dataPedido);
		                    location.reload();
		                }
				  	},
				  	error: function(XMLHttpRequest, textStatus, errorThrown) {
				  		console.log([XMLHttpRequest, textStatus, errorThrown]);
				     	alert("Los datos se almacenaran en local para ser guardados posteriormente ");
				     	addLocalStorage(dataPedido);
				  	}
				});
			}else{
				$('.list_areas, .list_funcionarios').attr("disabled",false);
				alert('Selecione el usuario que realiza el pedido');
			}
        }

        function limpiar_pedido(){
        	 $('.list_funcionarios option:eq(0)').prop('selected', true).attr("disabled",true);
             $('.list_areas option:eq(0)').prop('selected', true);
             $('.list_areas').attr("disabled",false);
             $('.desc_prod option:eq(0)').prop('selected', true);
             $('.agregar_prod, .enviar_ped, .list_funcionarios, .desc_prod').attr("disabled",true);
             $('.eliminar_pedido').closest('tr.prod_ped').remove();
             $('#total_id').html(0); 
             $('#subtotal, #precio_u').text("");
             $('#id_usuario, #search_user').val("");

        }

        function send_mail(dataPedido){
        	$.ajax({
			  	type: "POST",
			  	url: "send_email_pedido.php",
			  	data: dataPedido,
			  	beforeSend: function( xhr ) {
	                $('div.cargando').removeClass('hide');
	            },
			  	success: function(data){
					$('div.cargando').addClass('hide');
				},
			});
        }

        function addLocalStorage(data){
        	if("pedidos" in localStorage){
        		console.log();
			} else {
			   localStorage.setItem("pedidos",'');
			}
        	var pedidos = localStorage.getItem("pedidos");
        	var nuevoPedido = JSON.stringify(data);
        	var nuevoStorage = pedidos + nuevoPedido + "/**/";
        	localStorage.setItem("pedidos", nuevoStorage);
        	limpiar_pedido();
        }
	    

	// MOSTRAR PEDIDOS POR USUARIO Y FECHA -->
	    
        function pedidos_usuario_fecha() {
            var x_user=$("#usuarioSeleccionado").val();
            var f_ini=$(".f_inicio").val();
            var f_fin=$(".f_fin").val();
            //alert(f_ini);
            //CONFIGURAR PARA CARGAR VISTA DE SOLO LA BUSQUEDA INDICADA SIN CEDULA
            //-----------------------------------------------------------------------
            if (!x_user) {

                //alert("Vacio: "+x_user);

                //registro_pagos("Vacio:"+x_user);

                $("#lista_pago_func").css("display", "none");
                $(".volver_ing_pag").css("display", "none");
                $("#lista_pago_func_fecha").css("display", "block");
                $(".volver_ped").css("display", "block");
                
                $("#t_body_lpendientes_original").children().remove();

                alert(x_user+" fecha_ini:"+f_ini+" fecha_fin:"+f_fin);

                $.post("get_ped_us.php",{user:x_user,fecha_ini:f_ini,fecha_fin:f_fin, bandera:'con_fecha_rango'},function(dato1,status){
                    row_dato1=dato1.split(";");

                    for (var i = 0; i < row_dato1.length; i++) {
                        var row_dato1_f=row_dato1[i].split("|");
                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                            //alert(row_dato1_f[0]);
                            $("#t_body_lpendientes_original").append("<tr id=\"pendiente-"+row_dato1_f[0]+"\"><td class=\"id_ped_fecha\">"+row_dato1_f[0]+"</td><td>"+row_dato1_f[1]+" "+row_dato1_f[2]+"</td><td class=\"id_fecha_fecha\">"+row_dato1_f[3]+"</td><td class='subtotal_ped_fun_busq pendiente-"+row_dato1_f[0]+"'>"+row_dato1_f[4]+"</td><td class='acciones'><img class=\"pagar\" src=\"../images/icon_svg/money175.svg\" width=\"25em\" height=\"25em\" title=\"Realizar Pago\">    <img class=\"detalle_ped\" src=\"../images/icon_svg/write13.svg\" width=\"25em\" height=\"25em\" title=\"Ver Detalles\"></td></tr>");
                            break;
                        }
                    };
                    calcula_total_fun_busq();
                });
            }else{
                //alert("Con algo: "+x_user);
                //registro_pagos(x_user);


                $("#lista_pago_func").css("display", "none");
                $(".volver_ing_pag").css("display", "none");
                $("#lista_pago_func_fecha").css("display", "block");
                $(".volver_ped").css("display", "block");
                
                $("#t_body_lpendientes_original").children().remove();

                $.post("get_ped_us.php",{user:x_user,fecha_ini:f_ini,fecha_fin:f_fin, bandera:'con_fecha'},function(dato1,status){
                    row_dato1=dato1.split(";");

                    for (var i = 0; i < row_dato1.length; i++) {
                        var row_dato1_f=row_dato1[i].split("|");
                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                            //alert(row_dato1_f[0]);
                            $("#t_body_lpendientes_original").append("<tr id=\"pendiente-"+row_dato1_f[0]+"\"><td class=\"id_ped_fecha\">"+row_dato1_f[0]+"</td><td>"+row_dato1_f[1]+" "+row_dato1_f[2]+"</td><td class=\"id_fecha_fecha\">"+row_dato1_f[3]+"</td><td class='subtotal_ped_fun_busq pendiente-"+row_dato1_f[0]+"'>"+row_dato1_f[4]+"</td><td><img class=\"pagar\" src=\"../images/icon_svg/money175.svg\" width=\"25em\" height=\"25em\" title=\"Realizar Pago\">    <img class=\"detalle_ped\" src=\"../images/icon_svg/write13.svg\" width=\"25em\" height=\"25em\" title=\"Ver Detalles\"></td></tr>");
                            break;
                        }
                    };

                    calcula_total_fun_busq();
                });
            }//fin else
        }

	    
        function pedidos_usuario(id,soli,fecha) {
            var x_id=id;
            var solicitud=soli;
            var fecha_id=fecha;

            if(solicitud=='ped'){
                $(".id_detal").html("#"+x_id+" - Fecha "+fecha_id);
                //alert(solicitud);

                $.post("get_ped_us.php",{user:x_id, bandera:'sin_fecha'},function(dato1,status){
                    row_dato1=dato1.split(";");

                    for (var i = 0; i < row_dato1.length; i++) {

                        var row_dato1_f=row_dato1[i].split("|");

                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                        	$('.titulo-pedido-func').text('Pedidos del funcionario '+row_dato1_f[1]+" "+row_dato1_f[2])
                            $("#body_t_pedido_usuario").append("<tr><td class=\"id_ped_fun\">"+row_dato1_f[0]+"</td><td class=\"id_fecha_fun\">"+row_dato1_f[3]+"</td><td class=\"subtotal_ped_fun_busq\">$"+parseFloat(row_dato1_f[4], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+"</td><td><img class=\"detalle_prod_fun\" src=\"../images/icon_svg/write13.svg\" width=\"20em\" height=\"20em\" title=\"Ver Detalles de Pedido\"> <img class='eliminar_pedido_fun left1' src=\"../images/icon_svg/delete84.svg\" width=\"20em\" height=\"20em\" title=\"Eliminar Pedido\"></td></tr>");
                            break;
                        }
                    };
                    calcula_total_fun_busq();
                });
            }

            if(solicitud=='prod'){
                $(".id_detal").html("#"+x_id+" - Fecha "+fecha_id);

                $.post("get_ped_us.php",{id_ped:x_id, bandera:'det_prod'},function(dato1,status){
                    row_dato1=dato1.split(";");

                    for (var i = 0; i < row_dato1.length; i++) {
                        var row_dato1_f=row_dato1[i].split("|");
                        for (var x = 0; x < (row_dato1_f.length-1); x++) {
                            $("#body_t_producto").append("<tr><td>"+row_dato1_f[0]+"</td><td id='calcula_total_ped'>"+row_dato1_f[1]+"</td><td>"+row_dato1_f[2]+"</td></tr>");
                            break;
                        }
                    };
                    calcula_total_fun_busq();
                });
            }
        }

        function registro_pagos(id) {
            $.post("reg_pag.php",{id_fun:id},function(datos){
                //alert(datos);
                row_datos=datos.split(";");
                   
                //for (var i = 0; i < row_datos.length; i++) {
                for (var i = 0; i < row_datos.length; i++) {
                    var row_dato1_f=row_datos[i].split(",");
                    
                     for (var z = 0; z < row_dato1_f.length; z++) {
                        var row_dato1_f2=row_dato1_f[z].split("|");
                    
                        for (var x = 0; x < (row_dato1_f2.length-1); x++) {
                            $("#body_t_reg_pagos").append("<tr><td>"+(i+1)+"</td><td>"+row_dato1_f2[0]+"</td><td>$"+parseFloat(row_dato1_f2[1], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString()+"</td><td>"+row_dato1_f[1]+"</td></tr>");
                            //alert(row_dato1_f2[1]);
                            break;
                        }
                    };
                };
            });
        }
    	</script>

    	
        <title>FodeMag</title>
        <link rel="icon" type="ico" href="../images/expresso.ico" />
    </head>