<?php 
	session_start();
	if (!isset($_SESSION["id_sesion"])){
	    header("Location:index.php");
	}elseif ($_SESSION['id_sesion'] == 'fodemag' || $_SESSION['id_sesion'] == 'admin') {
	    include 'header.php';
	    include 'conn.php';
	    date_default_timezone_set('America/Bogota');
?>
<div class="row">
	<div class="col-xs-8 col-xs-offset-2">
		<div class="panel panel-primary">
				<div class="panel-heading">Actualizar contraseñas </div>
					<?php 
				$sql = ($_SESSION['id_sesion'] == 'fodemag') ? "SELECT * FROM usuarios WHERE user != 'admin' " : "SELECT * FROM usuarios" ; 
				$result = mysqli_query($conn, $sql);
  				$usuarios = mysqli_num_rows($result); ?>
  				<div class="row" style="padding: 1em">
  				<?php 
				while ($usuario = mysqli_fetch_array($result)) { ?>
						<div class="col-sm-6 col-md-4">
							<div class="thumbnail text-center">
								<div id="target-icon">
									<?= $usuario['icon']; ?>
								</div>
								<div class="caption">
									<h3><?= $usuario['user'] ?></h3>
									<form class="update_pass_user" id="user_<?= $usuario['id_usuario'] ?>">
										<div class="input-group" style="margin-top: 1em">
											<input type="text" required="" name="pass" autocomplete="off" class="form-control" placeholder="Actualizar contraseña">
											<span class="input-group-btn">
												<button class="btn btn-success" type="submit">Actualizar</button>
											</span>
										</div>
										<input required="" id="id_usuario" value="<?= $usuario['id_usuario'] ?>" type="hidden" autocomplete="off" name="id_usuario" class="form-control heigth2">
									</form>
								</div>
							</div>
						</div>
			  	<?php } ?>
			  	</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<div id="precarga" class="hide">
			
		</div>
	</div>
	<div class="col-xs-12 text-center">
		<div id="precarga" class="" style="margin-top: 2em">
			<span class="red hide" id="message" style="background: white; font-size: 2em; padding: .5em 1em; "></span>
		</div>
	</div>
</div>
<?php }else{
	header("Location:index.php");
} ?>
