<style>
	th{
		text-align: center;
	}
</style>
<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}else{ 
date_default_timezone_set('America/Bogota');
?>
<!DOCTYPE HTML>
	<html>
	<head>
		<title>FodeMag - Opciones</title>
		<?php
			include 'header.php';
			include 'conn.php';
			$total1 = 0;
			$total2 = 0;
		?>
	</head>
	<body>
		<div style="vertical-align:middle; margin: 0; text-align: center; padding: 2em">
				
				<?php 

				$planta = $_REQUEST['pl'];
				$hoy = date('Y-m-d');

				if ($planta!="all") {
					
				 ?>
				<div class="row">
					<h4 class="titulo padding1">REPORTE DETALLES DE PEDIDOS DE HOY <b><?= $hoy?></b></h4>
					<div class="col-xs-5 col-xs-offset-3">
						<br><br>
						<?php 
							$total = 0;
								$sql0 = "SELECT p.fecha,p.pedido_planta,pp.id_producto, pr.descripcion, pp.precio_unidad,SUM(pp.cantidad) TotalCantidad, SUM( (pp.cantidad*pp.precio_unidad) ) as PrecioFinal
								FROM pedidos p 
								JOIN pedido_producto pp ON (p.id_pedido=pp.id_pedido) 
								JOIN productos pr ON(pp.id_producto=pr.id_producto) 
								WHERE p.fecha = '$hoy' AND p.pedido_planta = '$planta'
								GROUP BY p.fecha,p.pedido_planta,pp.id_producto,pr.descripcion,pp.precio_unidad
								ORDER BY `pr`.`descripcion`  ASC";

		                        //echo "$sql0";
							?>

						<table class="table table-hover table-striped">

							<th class="text-center">Número</th>
							<th class="text-center">Producto</th>
							<th class="text-center">Cantidad</th>
							<th class="text-center">Valor unidad</th>
							<th class="text-center">Acumulado</th>
							
							<?php 
								$cont = 1;
								$row = mysqli_query($conn,$sql0);

		                        while ($ventas = mysqli_fetch_array($row, MYSQLI_BOTH)) {
		                        	?>
		                        	<tr>
		                        		<td><?= $cont++; ?></td><?php
			                        	?><td><?= $ventas['descripcion']; ?></td><?php
			                        	?><td class="text-center"><?= $ventas['TotalCantidad']; ?></td><?php
			                        	?><td class="text-center"><?= $ventas['precio_unidad']; ?></td><?php
			                        	?><td class="text-center">$<?=  $ventas['precio_unidad']*$ventas['TotalCantidad']; ?></td>
			                        </tr>
			                        <?php
			                        $total += $ventas['precio_unidad']*$ventas['TotalCantidad'];
									//echo $total;
		                        }
							?>
						</table>
						<div class="text-center">
							<p class="bg-success padding1 bg-green"><label for="Total_ventas">Total Ventas: </label> $ <?= $total;?></p>
						</div>

						<div class="col-xs-4" style="margin-top: 1em;"><a href="pedidos.php"><button type="button" class="btn btn-default btn-lg btn-block">Volver</button></a></div>
					</div>
				</div>

				<?php 
				}else { ?>
					<div class="row">
						<div class="col-xs-6">
						<h4 class="titulo padding1">REPORTE DETALLES DE PEDIDOS DE HOY <b><?= $hoy?> PLANTA 1</b></h4>
							<br><br>
							<?php 
									$sql0 = "SELECT p.fecha,p.pedido_planta,pp.id_producto, pr.descripcion, pp.precio_unidad,SUM(pp.cantidad) TotalCantidad, SUM( (pp.cantidad*pp.precio_unidad) ) as PrecioFinal
								FROM pedidos p 
								JOIN pedido_producto pp ON (p.id_pedido=pp.id_pedido) 
								JOIN productos pr ON(pp.id_producto=pr.id_producto) 
								WHERE p.fecha = '$hoy' AND p.pedido_planta = 'PL1' 
								GROUP BY p.fecha,p.pedido_planta,pp.id_producto,pr.descripcion,pp.precio_unidad
								ORDER BY `pr`.`descripcion`  ASC";
			                    //echo "$sql0";
								?>

							<table class="table table-hover table-striped">

								<th class="text-center">Número</th>
								<th class="text-center">Producto</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">Valor unidad</th>
								<th class="text-center">Acumulado</th>
								
								<?php 
									$row = mysqli_query($conn,$sql0);
									$cont = 1;

			                        while ($ventas = mysqli_fetch_array($row, MYSQLI_BOTH)) {
			                        	?>
			                        	<tr>
			                        		<td><?= $cont++; ?></td><?php
				                        	?><td><?= $ventas['descripcion']; ?></td><?php
				                        	?><td class="text-center"><?= $ventas['TotalCantidad']; ?></td><?php
				                        	?><td class="text-center"><?= $ventas['precio_unidad']; ?></td><?php
				                        	?><td class="text-center">$<?= $ventas['precio_unidad']*$ventas['TotalCantidad']; ?></td>
				                        </tr>
				                        <?php
				                        $total1 += $ventas['precio_unidad']*$ventas['TotalCantidad'];
			                        }
								?>
							</table>
							<div class="text-center">
								<p class="bg-success padding1 bg-green"><label for="Total_ventas">Total Ventas: </label> $ <?= $total1;?></p>
								
							</div>
						</div>


						<div class="col-xs-6">
							<h4 class="titulo padding1">REPORTE DETALLES DE PEDIDOS DE HOY <b><?= $hoy?> PLANTA 2</b></h4>
							<br><br>
							<?php 
									$sql1 =  "SELECT p.fecha,p.pedido_planta,pp.id_producto, pr.descripcion, pp.precio_unidad,SUM(pp.cantidad) TotalCantidad, SUM( (pp.cantidad*pp.precio_unidad) ) as PrecioFinal
								FROM pedidos p 
								JOIN pedido_producto pp ON (p.id_pedido=pp.id_pedido) 
								JOIN productos pr ON(pp.id_producto=pr.id_producto) 
								WHERE p.fecha = '$hoy' AND p.pedido_planta = 'PL2' 
								GROUP BY p.fecha,p.pedido_planta,pp.id_producto,pr.descripcion,pp.precio_unidad
								ORDER BY `pr`.`descripcion`  ASC"
			                        //echo "$sql0";
								?>

							<table class="table table-hover table-striped">

								<th class="text-center">Número</th>
								<th class="text-center">Producto</th>
								<th class="text-center">Cantidad</th>
								<th class="text-center">Valor unidad</th>
								<th class="text-center">Acumulado</th>
								
								<?php 
									$row1 = mysqli_query($conn,$sql1);
									$cont = 1;

			                        while ($ventas1 = mysqli_fetch_array($row1, MYSQLI_BOTH)) {
			                        	?>
			                        	<tr>
			                        		<td><?= $cont++; ?></td><?php
				                        	?><td><?= $ventas1['descripcion']; ?></td><?php
				                        	?><td class="text-center"><?= $ventas1['TotalCantidad']; ?></td><?php
				                        	?><td class="text-center"><?= $ventas1['precio_unidad']; ?></td><?php
				                        	?><td class="text-center">$<?= $ventas1['precio_unidad']*$ventas1['TotalCantidad']; ?></td>
				                        </tr>
				                        <?php
				                        $total2 += $ventas1['precio_unidad']*$ventas1['TotalCantidad'];
			                        }
								?>
							</table>
							<div class="text-center">
								<p class="bg-success padding1 bg-green"><label for="Total_ventas">Total Ventas: </label> $ <?= $total2;?></p>
							</div>
						</div>

					</div>
						
				<?php 
				}
				?>
			</div>

		<?php include 'footer.php' ?>
		</div>
	</body>
	</html>
	<?php
	} 
?>