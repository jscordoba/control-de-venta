<style type="text/css">
::-webkit-scrollbar {
      width: 15px;
}
::-webkit-scrollbar-button {
      background-color: transparent;
}
::-webkit-scrollbar-corner {
      background-color: transparent;
}
::-webkit-scrollbar-thumb {
      background-color: rgba(0, 0, 0, .1);
      border-radius: 10px;
      height: 4em;
} 
.db_local{
	position: absolute;
	top: 15px;
	right: 10px;
	z-index: 3;
}
</style>
<?php session_start(); 
if (!isset($_SESSION["id_sesion"])){ 
   header("Location:index.php");
}else{  ?>
<!DOCTYPE HTML>
	<html>
		<head>
			<title>FodeMag - Opciones</title>
			<?php
				include 'header.php';
				include 'conn.php';
				$total1 = 0;
				$total2 = 0;
			?>
		</head>
		<script type="text/javascript">
			 $(document).ready(function(){
			 	pedidosList = {};
			 	arrayPedidosList = [];
			 	if("pedidos" in localStorage){
	              	var datosLocal = localStorage.getItem("pedidos");
	              	pedidosList = datosLocal.split("/**/");
	              	pedidosList.pop();
	              	$.each( pedidosList, function( key, value ) {
	              			arrayPedidosList.push(value);
							var pedidos = JSON.parse(value);
							var cantidades = pedidos['cantidades'].split(",");
							var htmlTabla = 
							'<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4" style="border-top: 1px solid  rgba(0,0,0,.1);  max-height: 15em !important;  min-height: 15em !important;  overflow: scroll; padding-top: 3em">\
								<button class="btn btn-success db_local" pedido="'+key+'" >Guardar</button>\
							<h4> Cedula: '+pedidos.id_user+' </h4>\
							<div class="table-responsive" >\
							  	<table class="table table-hover" style="border: 1px solid rgba(0,0,0,.1); max-height: 2em !important;  min-height: 2em !important;  overflow: hidden">\
							    	<thead>\
										<tr>\
											<th>Producto</th>\
											<th>Cantidad</th>\
											<th>Acumulado</th>\
										</tr>\
									</thead>\
									<tbody id="tabla-'+key+'" >\
									</tbody>\
							  	</table>\
							</div>\
						</div>';
						$('#pedidosListStorage').append(htmlTabla);
						$.each( pedidos['descripciones'], function( index, producto ) {
							var datoFila = 
							'<tr>\
								<td>'+producto+'</td>\
								<td>'+cantidades[index]+'</td>\
								<td>$ '+parseInt(pedidos.valor_compras[index]) * parseInt(cantidades[index])+'</td>\
							</tr>';
							$('#tabla-'+key).append(datoFila);
						});
					});
					eliminarEventos();
	            } 
			});



			function eliminarEventos(){
			 	$(".db_local").off('click');
			 	$("#deleteAlmacenamientoLocal").off('click');
			 	recargarEventos();
			}

			function recargarEventos(){
			 	$(".db_local").on('click',function(){
			 		pedidoActual = $(this).attr('pedido');
			 		guardar_pedido(JSON.parse(pedidosList[pedidoActual]), pedidoActual);
			 		$(this).attr("disabled",true);
			 		$(this).closest('div').remove();
			 	});
			 	$("#deleteAlmacenamientoLocal").click( function(event) {
	              event.preventDefault();
	              var paso = confirm("desea elimiar el pedido");
	              if (paso == true) {
	              	localStorage.removeItem('pedidos');
	              	location.reload();
	              }
	            });
			}

			function guardar_pedido(dataPedido,pedidoActual){
				$(".db_local").attr("disabled",true);
				$.ajax({
				  	type: "POST",
				  	url: "ins_ped.php",
				  	data: dataPedido,
				  	success: function(data){
						var id = data.split(",");
						var id = parseInt(id[0]);
						dataPedido.idPedido = id;
				        if (id > 0) {
							send_mail(dataPedido);
	                    	alert("Se ha registrado la compra con éxito.");
	                    	updateStorage(pedidoActual);
		                }else{
		                    alert("Error al Registrar el Pedido");
		                }
				  	},
				  	error: function(XMLHttpRequest, textStatus, errorThrown) {
				     	alert("Dato no almacenado no se borrara de registro local ");
				  	}
				});
			}

			function updateStorage(pedidoActual){
				var stringLocal = '';
				arrayPedidosList.splice(pedidoActual ,1);
				$.each( arrayPedidosList, function( key, value ) {
					var jsonNewPedidos = JSON.parse(value);
					var stringNewPedidos = JSON.stringify(jsonNewPedidos);
					stringLocal = stringLocal + stringNewPedidos + "/**/";
				});

				localStorage.removeItem("pedidos");
				localStorage.setItem("pedidos",stringLocal);
				$(".db_local").attr("disabled",false);
			}

			
		</script>
		<body>
			<div style="padding: 4em">
				<h3 class="text-center " style="margin: 1em 0em 2em 0em">Listado de pedidos almacenados en local</h3>
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-right">
							<button class="btn btn-danger" id="deleteAlmacenamientoLocal" >Eliminar todo el almacenamiento local</button>
						</div>
					</div>
					<div class="row">
						<div id="pedidosListStorage">

						</div>
					</div>
				</div>
			</div>
		</body>
	</html>
<?php } ?>